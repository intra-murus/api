FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD /build/libs/intramurus-api*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=dev","-jar","/app.jar"]
