# Intramurus API
## Running locally
##### 1. Download Wildfly (take the one in rest-mybatis gradle project) and save it somewhere on your pc
##### 2. Click on the drop down next to the play button at the top right corner of IntelliJ and select Edit Configuration
##### 2. Press the + button and create a run configuration for JBoss wildfly
##### 3. Click configure and set the path to your wildfly
##### 4. Under libraries, click the + and add all the files under your-wildfly-path\modules\system\layers\base\javax\servlet\api\main\*
##### 2. Under Environment, type the following line in the VM options field (without the quotes)
"-Dspring.profiles.active=local"
##### 3. Run and go to http://localhost:8080/intramurus-api/swagger-ui.html
## Using the deployed version
#### Deployment
##### 1. Do a gradle clean and build
##### 2. Take the intramurus-api.war file in build/libs and pu it on zeus in /home/s6infoe01/
##### 3. Right click and go to properties
##### 3. Change the permissions to 0640
##### 4. Delete the .failed file
##### 5. Wait for the .deployed file to appear then you are done
##### 6. Go to http://zeus.gel.usherbrooke.ca:8080/intramurus-api/swagger-ui.html