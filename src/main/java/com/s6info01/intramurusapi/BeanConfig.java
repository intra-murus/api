package com.s6info01.intramurusapi;

import com.s6info01.intramurusapi.authentication.TokenFilter;
import org.apache.logging.log4j.util.Strings;
import org.jasig.cas.client.authentication.AuthenticationFilter;
import org.jasig.cas.client.util.HttpServletRequestWrapperFilter;
import org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class BeanConfig {

    @Bean
    public FilterRegistrationBean<TokenFilter> trustFilter(){
        FilterRegistrationBean<TokenFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.setFilter(new TokenFilter());
        registrationBean.addUrlPatterns("/api/*");

        return registrationBean;
    }

    @Bean
    @Autowired
    public FilterRegistrationBean<AuthenticationFilter> casAuthenticationFilter(final IntramurusProperties properties){
        FilterRegistrationBean<AuthenticationFilter> registrationBean
                = new FilterRegistrationBean<>();

        Map<String, String> params = new HashMap<>();
        params.put("casServerLoginUrl", "https://cas.usherbrooke.ca/login");
        params.put("serverName", properties.getApiEndpoint());

        registrationBean.setInitParameters(params);
        registrationBean.addUrlPatterns("/login");
        registrationBean.setFilter(new AuthenticationFilter());

        return registrationBean;
    }

    @Bean
    @Autowired
    public FilterRegistrationBean<Cas30ProxyReceivingTicketValidationFilter> csaTicketFilter(final IntramurusProperties properties){
        FilterRegistrationBean<Cas30ProxyReceivingTicketValidationFilter> registrationBean
                = new FilterRegistrationBean<>();

        Map<String, String> params = new HashMap<>();
        params.put("casServerUrlPrefix", "https://cas.usherbrooke.ca");
        params.put("serverName", properties.getApiEndpoint());

        registrationBean.setInitParameters(params);
        registrationBean.addUrlPatterns("/login");
        registrationBean.setFilter(new Cas30ProxyReceivingTicketValidationFilter());

        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<HttpServletRequestWrapperFilter> wrapperFilter(){
        FilterRegistrationBean<HttpServletRequestWrapperFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.addUrlPatterns("/login");
        registrationBean.setFilter(new HttpServletRequestWrapperFilter());

        return registrationBean;
    }
}
