package com.s6info01.intramurusapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
@MapperScan("com/s6info01/intramurusapi/persistence")
public class IntramurusApiApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(IntramurusApiApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(IntramurusApiApplication.class, args);
    }
}