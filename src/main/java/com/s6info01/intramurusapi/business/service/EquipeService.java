package com.s6info01.intramurusapi.business.service;

import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.types.*;
import com.s6info01.intramurusapi.exceptions.MembreAlreadyInLigueException;
import com.s6info01.intramurusapi.exceptions.MyBatisException;
import com.s6info01.intramurusapi.exceptions.ResourceNotFoundException;
import com.s6info01.intramurusapi.persistence.EquipeMapper;
import com.s6info01.intramurusapi.persistence.MembreEquipeMapper;
import com.s6info01.intramurusapi.persistence.MembreMapper;
import com.s6info01.intramurusapi.persistence.ViewEquipeMapper;
import com.s6info01.intramurusapi.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EquipeService {

    private final EquipeMapper equipeMapper;
    private final MembreMapper membreMapper;
    private final MembreEquipeMapper membreEquipeMapper;
    private final ViewEquipeMapper viewEquipeMapper;

    @Autowired
    public EquipeService(final EquipeMapper equipeMapper,
                         final MembreEquipeMapper membreEquipeMapper,
                         final ViewEquipeMapper viewEquipeMapper,
                         final MembreMapper membreMapper) {
        this.equipeMapper = equipeMapper;
        this.membreEquipeMapper = membreEquipeMapper;
        this.viewEquipeMapper = viewEquipeMapper;
        this.membreMapper = membreMapper;
    }

    public Equipe getEquipeById(final Long id) {
        Equipe equipe = equipeMapper.findById(id);
        if(equipe == null) {
            throw new ResourceNotFoundException("equipe : id = " + id);
        }
        return equipe;
    }

    public List<Equipe> getEquipesByCip(final String cip) {
        return equipeMapper.findByCip(cip);
    }

    public List<Membre> getMembresInEquipe(final Long idEquipe) {
        return viewEquipeMapper.getCipsInEquipe(idEquipe)
                .stream()
                .map(membreMapper::findOne)
                .collect(Collectors.toList());
    }

    public List<MembreInEquipe> getMembresEquipeInfo(final Long idEquipe) {
        return viewEquipeMapper.getMembresEquipeInfo(idEquipe);
    }

    public List<Integer> getDistinctIdEquipes(final EPeriode periode,
                                              final Integer annee,
                                              final String sport,
                                              final String nomLigue,
                                              final EStatutApprobation statutApprobation) {
        if(Strings.isNullOrEmpty(sport)) {
            return viewEquipeMapper.getDistinctIdEquipesAllSports(periode, annee, statutApprobation);
        } else if(Strings.isNullOrEmpty(nomLigue)) {
            return viewEquipeMapper.getDistinctIdEquipesAllLigues(periode, annee, sport, statutApprobation);
        } else {
            return viewEquipeMapper.getDistinctIdEquipes(periode, annee, sport, nomLigue, statutApprobation);
        }
    }

    public Integer countMembersInEquipes(final Integer idEquipe) {
        return viewEquipeMapper.countMembersInEquipe(idEquipe);
    }

    public List<EquipeView> getApprobationView(final EPeriode periode,
                                               final Integer annee,
                                               final String sport,
                                               final String nomLigue,
                                               final EStatutApprobation statutApprobation) {

        List<EquipeView> membres;

        if(Strings.isNullOrEmpty(sport)){
            membres = viewEquipeMapper.findAllSports(periode, annee, statutApprobation);
        } else if(Strings.isNullOrEmpty(nomLigue)) {
            membres = viewEquipeMapper.findAllLigues(periode, annee, sport, statutApprobation);
        } else {
            membres = viewEquipeMapper.findByFilters(periode, annee, sport, nomLigue, statutApprobation);
        }

        List<EquipeView> membresFinal = membres;

        List<Integer> distinctEquipesId = getDistinctIdEquipes(periode, annee, sport, nomLigue, statutApprobation);

        List<List<EquipeView>> equipesFilteredList = new ArrayList<>();

        distinctEquipesId.forEach((idEquipe) -> {
            List<EquipeView> equipe = new ArrayList<>();

            membresFinal.forEach((membre) -> {
               if((membre.getIdEquipe() == idEquipe)) {
                   equipe.add(membre);
               }
            });

            Integer memberCount = countMembersInEquipes(idEquipe);

            if(memberCount == equipe.size()) {
                equipesFilteredList.add(equipe);
            }
        });

        List<EquipeView> equipesFiltered = new ArrayList<>();

        equipesFilteredList.forEach((equipeFiltered) -> equipeFiltered.forEach(equipesFiltered::add));

        return equipesFiltered;
    }

    public void updateStatutAppbrobation(final Long id,
                                         final EStatutApprobation statutApprobation) {
        Equipe equipe = getEquipeById(id);
        equipe.setStatutApprobation(statutApprobation);
        if(equipeMapper.update(equipe) == 0) {
            throw new MyBatisException("update equipe : " + equipe);
        }
    }

    public void updateStatutMembreEquipe(final String cip,
                                         final Long id,
                                         final EStatutMembreEquipe statutMembreEquipe) {
        MembreEquipe membreEquipe = membreEquipeMapper.findOne(cip, id);
        if(membreEquipe == null) {
            throw new ResourceNotFoundException("membreEquipe : cip = " + cip + ", id_equipe = " + id);
        }
        membreEquipe.setStatutMembreEquipe(statutMembreEquipe);
        if(membreEquipeMapper.update(membreEquipe) == 0) {
            throw new MyBatisException("update membreEquipe : " + membreEquipe);
        }
    }

    public Equipe createEquipe(final String sport,
                               final String nomLigue,
                               final String nomEquipe,
                               final String cipCapitaine,
                               final List<String> cipsJoueurs) {

        List<String> cipsEquipe = new ArrayList<>(cipsJoueurs);
        if(!cipsEquipe.contains(cipCapitaine)) {
            cipsEquipe.add(cipCapitaine);
        }

        for(final String cipJoueur : cipsEquipe) {
            Membre membre = membreMapper.findOne(cipJoueur);
            if(membre == null) {
                throw new ResourceNotFoundException("cip = " + cipJoueur);
            }
            if(!viewEquipeMapper.getEquipeInLigueForMembre(cipJoueur,
                    SessionUtil.getCurrentPeriode(),
                    LocalDate.now().getYear(),
                    sport,
                    nomLigue).isEmpty()) {
                throw new MembreAlreadyInLigueException("cip = " + cipJoueur + ", ligue = " + nomLigue);
            }
        }

        // add equipe in database
        Equipe equipe = new Equipe();
        equipe.setStatutApprobation(EStatutApprobation.EN_ATTENTE);
        equipe.setSport(sport);
        equipe.setPeriode(SessionUtil.getCurrentPeriode());
        equipe.setAnnee(LocalDate.now().getYear());
        equipe.setNomLigue(nomLigue);
        equipe.setNomEquipe(nomEquipe);
        equipe.setCipCapitaine(cipCapitaine);
        if(equipeMapper.insert(equipe) == 0) {
            throw new MyBatisException("insert equipe " + equipe);
        }

        // link equipe to membre
        for(final String cipJoueur : cipsEquipe) {
            MembreEquipe membreEquipe = new MembreEquipe();
            membreEquipe.setCip(cipJoueur);
            membreEquipe.setIdEquipe(equipe.getIdEquipe());
            membreEquipe.setStatutMembreEquipe(EStatutMembreEquipe.EN_ATTENTE);
            membreEquipeMapper.insert(membreEquipe);
        }

        return equipe;
    }
}
