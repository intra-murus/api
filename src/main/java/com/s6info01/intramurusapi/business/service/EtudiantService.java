package com.s6info01.intramurusapi.business.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.types.Etudiant;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class EtudiantService {
    public Etudiant getEtudiantInfo(final String cip) {

        Etudiant etudiant = new Etudiant();
        etudiant.setCip_etudiant(cip);

        try {

            URL url = new URL("http://zeus.gel.usherbrooke.ca:8080/ms/rest/etudiant_groupe?inscription=2017-01-01&trimestre_id=A18&cip_etudiant=" + cip);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


            String output = "";
            String line;
            while ((line = br.readLine()) != null) {
                output += line;
            }

            conn.disconnect();

            if(!Strings.isNullOrEmpty(output)) {
                JSONArray jsonArray = new JSONArray(output);
                etudiant = new ObjectMapper().readValue(jsonArray.get(0).toString(), Etudiant.class);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return etudiant;
    }
}
