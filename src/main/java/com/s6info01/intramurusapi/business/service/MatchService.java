package com.s6info01.intramurusapi.business.service;

import com.s6info01.intramurusapi.business.types.*;
import com.s6info01.intramurusapi.exceptions.ResourceNotFoundException;
import com.s6info01.intramurusapi.exceptions.MyBatisException;
import com.s6info01.intramurusapi.persistence.MatchMapper;
import com.s6info01.intramurusapi.persistence.ViewMatchMapper;
import com.s6info01.intramurusapi.persistence.ViewMembreMatchMapper;
import com.s6info01.intramurusapi.util.SessionUtil;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Service
public class MatchService {

    private final ViewMatchMapper viewMatchMapper;
    private final ViewMembreMatchMapper viewMembreMatchMapper;
    private final MatchMapper matchMapper;

    public MatchService(final ViewMatchMapper viewMatchMapper,
                        final ViewMembreMatchMapper viewMembreMatchMapper,
                        final MatchMapper matchMapper) {
        this.viewMatchMapper = viewMatchMapper;
        this.viewMembreMatchMapper = viewMembreMatchMapper;
        this.matchMapper = matchMapper;
    }

    public List<MatchView> getEquipeMatch(final Long matchId) {
        return viewMatchMapper.findByMatchId(matchId);
    }

    public List<MembreMatchView> getMembreMatch(final Long matchId) {
        return viewMembreMatchMapper.findByMatchId(matchId);
    }

    public Match getMatchById(final Long idMatch) {
        final Match match = matchMapper.findById(idMatch);
        if(match == null) {
            throw new ResourceNotFoundException("match : id = " + idMatch);
        }
        return match;
    }

    public List<MatchView> getMatchsByTerrain(final String terrain,
                                              final long startDate,
                                              final long endDate) {
        return viewMatchMapper.findMatchsByTerrain(terrain,
                startDate,
                endDate);
    }

    public void updateStatutMembreMatch(final Long idMatch,
                                        final String cip,
                                        final EStatutMembreMatch statutMembreMatch) {
        MembreMatchView membreMatchView = viewMembreMatchMapper.findByCipAndMatchId(idMatch, cip);
        if(membreMatchView == null) {
            throw new ResourceNotFoundException("membreMatchView : cip = " + cip + ", id_match = " + idMatch);
        }
        membreMatchView.setStatutMembreMatch(statutMembreMatch);
        if(viewMembreMatchMapper.updateStatutMembreMatch(membreMatchView) == 0) {
            throw new MyBatisException("update membreMatchView : " + membreMatchView);
        }
    }

    public void updateStatutEquipeMatch(final Long idEquipe,
                                        final Long idMatch,
                                        final EStatutEquipeMatch statutEquipeMatch) {
        MatchView matchView = viewMatchMapper.findByEquipeIdAndMatchId(idEquipe, idMatch);
        if(matchView == null) {
            throw new ResourceNotFoundException("matchView : idEquipe = " + idEquipe + ", id_match = " + idMatch);
        }
        matchView.setStatutEquipeMatch(statutEquipeMatch);
        if(viewMatchMapper.updateStatutEquipeMatch(matchView) == 0) {
            throw new MyBatisException("update matchView : " + matchView);
        }
    }

    public void updateMatch(final Long idMatch,
                            final Long dateDebut,
                            final Long dateFin) {
        Match match = getMatchById(idMatch);
        match.setDateDebut(Timestamp.from(Instant.ofEpochSecond(dateDebut)));
        match.setDateFin(Timestamp.from(Instant.ofEpochSecond(dateFin)));
        if(matchMapper.update(match) == 0) {
            throw new MyBatisException("update match, id: " + match.getIdMatch());
        }
    }

    public Match insertMatch(final Long idEquipeHome, final Long idEquipeAway, final Long dateDebut,final Long dateFin,
                            final String terrain, final String sport, final String nomLigue, final String arbitre,
                            final List<Membre> membresMatch) {
        Match match = new Match();
        match.setTerrain(terrain);
        match.setStatutMatch(EStatutMatch.EN_ATTENTE);
        match.setSport(sport);
        match.setPeriode(SessionUtil.getCurrentPeriode());
        match.setAnnee(LocalDate.now().getYear());
        match.setNomLigue(nomLigue);
        match.setCip(arbitre);
        match.setDateDebut(Timestamp.from(Instant.ofEpochSecond(dateDebut)));
        match.setDateFin(Timestamp.from(Instant.ofEpochSecond(dateFin)));

        // Insert into table match
        if(matchMapper.insert(match) == 0) {
            throw new MyBatisException("insert match : " + match);
        }

        // Insert into table EquipeMatch
        MatchView matchView = new MatchView();
        Long idMatch= match.getIdMatch();
        // Equipe Home
        matchView.setIdMatch(idMatch);
        matchView.setIdEquipe(idEquipeHome);
        matchView.setStatutEquipeMatch(EStatutEquipeMatch.EN_ATTENTE);
        matchView.setCote("HOME");
        viewMatchMapper.insertIntoEquipeMatch(matchView);
        // Equipe Away
        matchView.setIdEquipe(idEquipeAway);
        matchView.setCote("AWAY");
        viewMatchMapper.insertIntoEquipeMatch(matchView);

        // Insert into table MembreMatch
        membresMatch.forEach(membre -> {
            MembreMatchView membreMatchView = new MembreMatchView();
            membreMatchView.setCip(membre.getCip());
            membreMatchView.setIdMatch(idMatch);
            membreMatchView.setStatutMembreMatch(EStatutMembreMatch.EN_ATTENTE);
            if (viewMembreMatchMapper.insertIntoMembreMatch(membreMatchView) == 0){
                throw new MyBatisException("insert membreMatchView : " + membreMatchView);
            }
        });

        return match;
    }

    public void deleteMatch(final Long idMatch){
        // Delete from table MembreMatch
        List<MembreMatchView> membres = viewMembreMatchMapper.findByMatchId(idMatch);
        membres.forEach(membre -> {
            if (viewMembreMatchMapper.deleteFromMembreMatch(membre.getCip(), membre.getIdMatch()) == 0) {
                throw new MyBatisException("delete membreMatchView : " + membre);
            }
        });

        // Delete from table EquipeMatch
        List<MatchView> matchs =  viewMatchMapper.findByMatchId(idMatch);
        matchs.forEach(match -> {
            if (viewMatchMapper.deleteFromEquipeMatch(match.getIdEquipe(), match.getIdMatch()) == 0) {
                throw new MyBatisException("delete matchView : " + match);
            }
        });

        // Delete from table Match
        if (matchMapper.deleteById(idMatch) == 0) {
            throw new MyBatisException("delete match id: " + idMatch);
        }

    }
}
