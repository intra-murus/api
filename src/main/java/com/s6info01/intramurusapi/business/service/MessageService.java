package com.s6info01.intramurusapi.business.service;

import com.s6info01.intramurusapi.business.types.*;
import com.s6info01.intramurusapi.exceptions.BadRequestException;
import com.s6info01.intramurusapi.exceptions.MyBatisException;
import com.s6info01.intramurusapi.exceptions.NotCapitaineOfEquipeException;
import com.s6info01.intramurusapi.exceptions.ResourceNotFoundException;
import com.s6info01.intramurusapi.persistence.MembreMapper;
import com.s6info01.intramurusapi.persistence.MembreMessageMapper;
import com.s6info01.intramurusapi.persistence.MessageMapper;
import com.s6info01.intramurusapi.persistence.ViewMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    private final ViewMessageMapper viewMessageMapper;
    private final MembreMapper membreMapper;
    private final MessageMapper messageMapper;
    private final MembreMessageMapper membreMessageMapper;
    private final EquipeService equipeService;

    @Autowired
    public MessageService(final ViewMessageMapper viewMessageMapper,
                          final MessageMapper messageMapper,
                          final MembreMessageMapper membreMessageMapper,
                          final MembreMapper membreMapper,
                          final EquipeService equipeService) {
        this.viewMessageMapper = viewMessageMapper;
        this.messageMapper = messageMapper;
        this.membreMessageMapper = membreMessageMapper;
        this.membreMapper = membreMapper;
        this.equipeService = equipeService;
    }

    public List<MessageView> getMessagesByCip(final String cip) {
        return viewMessageMapper.findByCip(cip);
    }

    public void createInvitationEquipe(final Equipe equipe, final List<Membre> membres) {
        if(membres.isEmpty()) {
            return;
        }

        if(equipe.getIdEquipe() == 0) {
            throw new BadRequestException("invalid equipe : " + equipe);
        }

        final Membre capitaine = membreMapper.findOne(equipe.getCipCapitaine());
        if(capitaine == null) {
            throw new ResourceNotFoundException("cip = " + equipe.getCipCapitaine());
        }

        final Message invitation = createInvitationMessage(equipe.getNomEquipe(), equipe.getIdEquipe(), capitaine.getNom(), capitaine.getPrenom());
        if(messageMapper.insert(invitation) == 0) {
            throw new MyBatisException("insert message " + invitation);
        }

        for(final Membre membre : membres) {
            if(membre.getCip().equals(capitaine.getCip())) {
                continue;
            }
            if(membreMessageMapper.insert(membre.getCip(), invitation.getIdMessage()) == 0) {
                throw new MyBatisException(String.format("insert membreMessage, cip = %s id_message = %d", membre.getCip(), invitation.getIdMessage()));
            }
        }
    }

    private Message createInvitationMessage(final String nomEquipe,
                                            final Long idEquipe,
                                            final String nomCapitaine,
                                            final String prenomCapitaine) {
        String message = String.format("Invitation à rejoindre l'équipe %s. Le capitaine %s %s vous invite à rejoindre son équipe.",
                nomEquipe,
                prenomCapitaine,
                nomCapitaine);

        Message invitation = new Message();
        invitation.setText(message);
        invitation.setTypeMessage(ETypeMessage.INVITATION);
        invitation.setLien(idEquipe);

        return invitation;
    }

    public void createMessageToEquipe(final String cipCapitaine,
                                      final Long idEquipe,
                                      final String text) {
        final Equipe equipe = equipeService.getEquipeById(idEquipe);
        if(equipe == null) {
            throw new ResourceNotFoundException("equipe = " + idEquipe);
        }

        final Membre capitaine = membreMapper.findOne(equipe.getCipCapitaine());
        if(capitaine == null) {
            throw new ResourceNotFoundException("cip = " + equipe.getCipCapitaine());
        }

        if(!equipe.getCipCapitaine().equals(cipCapitaine)) {
            throw new NotCapitaineOfEquipeException("cip = " + cipCapitaine + ", equipe = " + equipe.getCipCapitaine());
        }

        Message message = new Message();
        message.setTypeMessage(ETypeMessage.TEXTE);
        message.setText(text);
        message.setLien(equipe.getIdEquipe());

        if(messageMapper.insert(message) == 0) {
            throw new MyBatisException("insert message " + message);
        }

        List<Membre> membresInEquipe = equipeService.getMembresInEquipe(equipe.getIdEquipe());
        membresInEquipe.stream()
                .filter(membre -> !membre.getCip().equals(equipe.getCipCapitaine()))
                .forEach(membre ->
            membreMessageMapper.insert(membre.getCip(), message.getIdMessage())
        );
    }

    public void deleteMembreMessage(final String cip,
                                    final Long idMessage) {
        membreMessageMapper.delete(cip, idMessage);
    }
}
