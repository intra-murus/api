package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EJourSemaine;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EJourSemaine.class)
public class JourSemaineTypeHandler extends BaseTypeHandler<EJourSemaine> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EJourSemaine parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getJourSemaine());
    }

    @Override
    public EJourSemaine getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EJourSemaine.fromValue(rs.getString(columnName));
    }

    @Override
    public EJourSemaine getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EJourSemaine.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EJourSemaine getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EJourSemaine.fromValue(cs.getString(columnIndex));
    }
}