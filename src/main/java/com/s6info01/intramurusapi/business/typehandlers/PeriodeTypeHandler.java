package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EPeriode;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EPeriode.class)
public class PeriodeTypeHandler extends BaseTypeHandler<EPeriode> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EPeriode parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getPeriode());
    }

    @Override
    public EPeriode getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EPeriode.fromValue(rs.getString(columnName));
    }

    @Override
    public EPeriode getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EPeriode.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EPeriode getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EPeriode.fromValue(cs.getString(columnIndex));
    }
}