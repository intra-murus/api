package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EStatutApprobation;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EStatutApprobation.class)
public class StatueApprobationTypeHandler extends BaseTypeHandler<EStatutApprobation> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EStatutApprobation parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getStatutApprobation());
    }

    @Override
    public EStatutApprobation getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EStatutApprobation.fromValue(rs.getString(columnName));
    }

    @Override
    public EStatutApprobation getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EStatutApprobation.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EStatutApprobation getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EStatutApprobation.fromValue(cs.getString(columnIndex));
    }
}