package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EStatutEquipeMatch;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EStatutEquipeMatch.class)
public class StatutEquipeMatchTypeHandler extends BaseTypeHandler<EStatutEquipeMatch> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EStatutEquipeMatch parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getStatutEquipeMatch());
    }

    @Override
    public EStatutEquipeMatch getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EStatutEquipeMatch.fromValue(rs.getString(columnName));
    }

    @Override
    public EStatutEquipeMatch getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EStatutEquipeMatch.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EStatutEquipeMatch getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EStatutEquipeMatch.fromValue(cs.getString(columnIndex));
    }
}