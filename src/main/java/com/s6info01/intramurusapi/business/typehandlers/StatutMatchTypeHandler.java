package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EStatutMatch;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EStatutMatch.class)
public class StatutMatchTypeHandler extends BaseTypeHandler<EStatutMatch> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EStatutMatch parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getStatutMatch());
    }

    @Override
    public EStatutMatch getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EStatutMatch.fromValue(rs.getString(columnName));
    }

    @Override
    public EStatutMatch getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EStatutMatch.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EStatutMatch getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EStatutMatch.fromValue(cs.getString(columnIndex));
    }
}