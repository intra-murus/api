package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EStatutMembreEquipe;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EStatutMembreEquipe.class)
public class StatutMembreEquipeTypeHandler extends BaseTypeHandler<EStatutMembreEquipe> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EStatutMembreEquipe parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getStatutMembreEquipe());
    }

    @Override
    public EStatutMembreEquipe getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EStatutMembreEquipe.fromValue(rs.getString(columnName));
    }

    @Override
    public EStatutMembreEquipe getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EStatutMembreEquipe.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EStatutMembreEquipe getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EStatutMembreEquipe.fromValue(cs.getString(columnIndex));
    }
}