package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EStatutMembreMatch;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(EStatutMembreMatch.class)
public class StatutMembreMatchTypeHandler extends BaseTypeHandler<EStatutMembreMatch> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, EStatutMembreMatch parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getStatutMembreMatch());
    }

    @Override
    public EStatutMembreMatch getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return EStatutMembreMatch.fromValue(rs.getString(columnName));
    }

    @Override
    public EStatutMembreMatch getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return EStatutMembreMatch.fromValue(rs.getString(columnIndex));
    }

    @Override
    public EStatutMembreMatch getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return EStatutMembreMatch.fromValue(cs.getString(columnIndex));
    }
}