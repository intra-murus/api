package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.ETypeDeMembre;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(ETypeDeMembre.class)
public class TypeDeMembreTypeHandler extends BaseTypeHandler<ETypeDeMembre> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, ETypeDeMembre parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getTypeDeMembre());
    }

    @Override
    public ETypeDeMembre getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return ETypeDeMembre.fromValue(rs.getString(columnName));
    }

    @Override
    public ETypeDeMembre getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return ETypeDeMembre.fromValue(rs.getString(columnIndex));
    }

    @Override
    public ETypeDeMembre getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return ETypeDeMembre.fromValue(cs.getString(columnIndex));
    }
}