package com.s6info01.intramurusapi.business.typehandlers;

import com.s6info01.intramurusapi.business.types.EJourSemaine;
import com.s6info01.intramurusapi.business.types.ETypeMessage;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(ETypeMessage.class)
public class TypeMessageTypeHandler extends BaseTypeHandler<ETypeMessage> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, ETypeMessage parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.getTypeMessage());
    }

    @Override
    public ETypeMessage getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return ETypeMessage.fromValue(rs.getString(columnName));
    }

    @Override
    public ETypeMessage getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return ETypeMessage.fromValue(rs.getString(columnIndex));
    }

    @Override
    public ETypeMessage getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return ETypeMessage.fromValue(cs.getString(columnIndex));
    }
}