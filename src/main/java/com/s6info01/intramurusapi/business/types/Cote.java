package com.s6info01.intramurusapi.business.types;

public class Cote {

  private String cote;
  private String couleurDossard;


  public String getCote() {
    return cote;
  }

  public void setCote(String cote) {
    this.cote = cote;
  }


  public String getCouleurDossard() {
    return couleurDossard;
  }

  public void setCouleurDossard(String couleurDossard) {
    this.couleurDossard = couleurDossard;
  }

}
