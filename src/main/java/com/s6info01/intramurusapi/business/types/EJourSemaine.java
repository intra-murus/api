package com.s6info01.intramurusapi.business.types;

public enum EJourSemaine {

    DIMANCHE("DIMANCHE"),
    LUNDI("LUNDI"),
    MARDI("MARDI"),
    MERCREDI("MERCREDI"),
    JEUDI("JEUDI"),
    VENDREDI("VENDREDI"),
    SAMEDI("SAMEDI");

    private String jourSemaine;

    EJourSemaine(String jourSemaine) {
    this.jourSemaine = jourSemaine;
  }

    public String getJourSemaine() {
        return jourSemaine;
    }

    public void setJourSemaine(String jourSemaine) {
        this.jourSemaine = jourSemaine;
    }
    public static EJourSemaine fromValue(String value){
        for (EJourSemaine item : EJourSemaine.values()){
          if (item.getJourSemaine().equals(value)){
            return item;
          }
        }
        return null;
    }

    @Override
    public String toString() {
    return jourSemaine;
    }
}
