package com.s6info01.intramurusapi.business.types;


public enum EPeriode {

    AUTOMNE("AUTOMNE"),
    HIVER("HIVER"),
    ETE("ETE");

    private String periode;

    EPeriode(String periode) {
        this.periode = periode;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public static EPeriode fromValue(String value){
        for (EPeriode item : EPeriode.values()){
            if (item.getPeriode().equals(value)){
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return periode;
    }
}
