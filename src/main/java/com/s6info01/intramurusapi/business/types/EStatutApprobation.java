package com.s6info01.intramurusapi.business.types;


public enum EStatutApprobation {

    APPROUVE("APPROUVE"),
    EN_ATTENTE("EN_ATTENTE"),
    PRET_A_APPROUVER("PRET_A_APPROUVER"),
    REFUSE("REFUSE");

    private String statutApprobation;

    EStatutApprobation(String statutApprobation) {
        this.statutApprobation = statutApprobation;
    }

    public String getStatutApprobation() {
    return statutApprobation;
    }

    public void setStatutApprobation(String statutApprobation) {
    this.statutApprobation = statutApprobation;
    }

    public static EStatutApprobation fromValue(String value){
        for (EStatutApprobation item : EStatutApprobation.values()){
            if (item.getStatutApprobation().equals(value)){
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return statutApprobation;
    }
}
