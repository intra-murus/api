package com.s6info01.intramurusapi.business.types;


public enum EStatutEquipeMatch {

    PRESENT("PRESENT"),
    EN_ATTENTE("EN_ATTENTE"),
    ABSENT("ABSENT");

    private String statutEquipeMatch;

    EStatutEquipeMatch(String statutEquipeMatch) {
    this.statutEquipeMatch = statutEquipeMatch;
    }

    public String getStatutEquipeMatch() {
        return statutEquipeMatch;
    }

    public void setStatutEquipeMatch(String statutEquipeMatch) {
        this.statutEquipeMatch = statutEquipeMatch;
    }

    public static EStatutEquipeMatch fromValue(String value){
        for (EStatutEquipeMatch item : EStatutEquipeMatch.values()){
          if (item.getStatutEquipeMatch().equals(value)){
            return item;
          }
        }
        return null;
    }

    @Override
    public String toString() {
    return statutEquipeMatch;
    }
}

