package com.s6info01.intramurusapi.business.types;


public enum EStatutMatch {

    CONFIRME("CONFIRME"),
    EN_ATTENTE("EN_ATTENTE"),
    REFUSE("REFUSE");

    private String statutMatch;

    EStatutMatch(String statutMatch) {
        this.statutMatch = statutMatch;
    }

    public String getStatutMatch() {
        return statutMatch;
    }

    public void setStatutMatch(String statutMatch) {
        this.statutMatch = statutMatch;
    }

    public static EStatutMatch fromValue(String value) {
        for (EStatutMatch item : EStatutMatch.values()) {
            if (item.getStatutMatch().equals(value)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return statutMatch;
    }
}
