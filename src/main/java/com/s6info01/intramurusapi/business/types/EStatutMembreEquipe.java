package com.s6info01.intramurusapi.business.types;


public enum EStatutMembreEquipe {

    ACCEPTE("ACCEPTE"),
    EN_ATTENTE("EN_ATTENTE"),
    REFUSE("REFUSE");

    private String statutMembreEquipe;

    EStatutMembreEquipe(String statutMembreEquipe) {
        this.statutMembreEquipe = statutMembreEquipe;
    }

    public String getStatutMembreEquipe() {
        return statutMembreEquipe;
    }

    public void setStatutMembreEquipe(String statutMembreEquipe) {
        this.statutMembreEquipe = statutMembreEquipe;
    }

    public static EStatutMembreEquipe fromValue(String value) {
        for (EStatutMembreEquipe item : EStatutMembreEquipe.values()) {
            if (item.getStatutMembreEquipe().equals(value)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return statutMembreEquipe;
    }
}
