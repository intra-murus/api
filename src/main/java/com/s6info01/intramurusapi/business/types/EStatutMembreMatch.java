package com.s6info01.intramurusapi.business.types;


public enum EStatutMembreMatch {

    PRESENT("PRESENT"),
    EN_ATTENTE("EN_ATTENTE"),
    ABSENT("ABSENT");

    private String statutMembreMatch;

    EStatutMembreMatch(String statutMembreMatch) {
        this.statutMembreMatch = statutMembreMatch;
    }

    public String getStatutMembreMatch() {
        return statutMembreMatch;
    }

    public void setStatutMembreMatch(String statutMembreMatch) {
        this.statutMembreMatch = statutMembreMatch;
    }

    public static EStatutMembreMatch fromValue(String value) {
        for (EStatutMembreMatch item : EStatutMembreMatch.values()) {
            if (item.getStatutMembreMatch().equals(value)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return statutMembreMatch;
    }
}
