package com.s6info01.intramurusapi.business.types;


public enum ETypeDeMembre {
    JOUEUR("JOUEUR"),
    ADMINISTRATEUR("ADMINISTRATEUR");

    private String typeDeMembre;

    ETypeDeMembre(String typeDeMembre) {
        this.typeDeMembre = typeDeMembre;
    }

    public String getTypeDeMembre() {
        return typeDeMembre;
    }

    public void setTypeDeMembre(String typeDeMembre) {
        this.typeDeMembre = typeDeMembre;
    }

    public static ETypeDeMembre fromValue(String value){
        for (ETypeDeMembre item : ETypeDeMembre.values()){
            if (item.getTypeDeMembre().equals(value)){
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return typeDeMembre;
    }
}
