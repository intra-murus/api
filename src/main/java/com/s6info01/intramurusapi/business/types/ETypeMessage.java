package com.s6info01.intramurusapi.business.types;


public enum ETypeMessage {

    INVITATION("INVITATION"),
    CONFIRMATION("CONFIRMATION"),
    RAPPEL("EN_ATTENTE"),
    TEXTE("TEXTE");

    private String typeMessage;

    ETypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public String getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public static ETypeMessage fromValue(String value) {
        for (ETypeMessage item : ETypeMessage.values()) {
            if (item.getTypeMessage().equals(value)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return typeMessage;
    }
}
