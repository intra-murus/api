package com.s6info01.intramurusapi.business.types;


public class EquipeMatch {

  private long idEquipe;
  private long idMatch;
  private String statutEquipeMatch;
  private String cote;


  public long getIdEquipe() {
    return idEquipe;
  }

  public void setIdEquipe(long idEquipe) {
    this.idEquipe = idEquipe;
  }


  public long getIdMatch() {
    return idMatch;
  }

  public void setIdMatch(long idMatch) {
    this.idMatch = idMatch;
  }


  public String getStatutEquipeMatch() {
    return statutEquipeMatch;
  }

  public void setStatutEquipeMatch(String statutEquipeMatch) {
    this.statutEquipeMatch = statutEquipeMatch;
  }


  public String getCote() {
    return cote;
  }

  public void setCote(String cote) {
    this.cote = cote;
  }

}
