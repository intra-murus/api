package com.s6info01.intramurusapi.business.types;


public class EquipeView {

  private String cip;
  private String nom;
  private String prenom;
  private String email;
  private EStatutMembreEquipe statutMembreEquipe;
  private long idEquipe;
  private EStatutApprobation statutApprobation;
  private String sport;
  private EPeriode periode;
  private long annee;
  private String nomLigue;
  private String nomEquipe;
  private String cipCapitaine;


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public EStatutMembreEquipe getStatutMembreEquipe() {
    return statutMembreEquipe;
  }

  public void setStatutMembreEquipe(EStatutMembreEquipe statutMembreEquipe) {
    this.statutMembreEquipe = statutMembreEquipe;
  }


  public long getIdEquipe() {
    return idEquipe;
  }

  public void setIdEquipe(long idEquipe) {
    this.idEquipe = idEquipe;
  }


  public EStatutApprobation getStatutApprobation() {
    return statutApprobation;
  }

  public void setStatutApprobation(EStatutApprobation statutApprobation) {
    this.statutApprobation = statutApprobation;
  }


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public EPeriode getPeriode() {
    return periode;
  }

  public void setPeriode(EPeriode periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public String getNomLigue() {
    return nomLigue;
  }

  public void setNomLigue(String nomLigue) {
    this.nomLigue = nomLigue;
  }


  public String getNomEquipe() {
    return nomEquipe;
  }

  public void setNomEquipe(String nomEquipe) {
    this.nomEquipe = nomEquipe;
  }


  public String getCipCapitaine() {
    return cipCapitaine;
  }

  public void setCipCapitaine(String cipCapitaine) {
    this.cipCapitaine = cipCapitaine;
  }

}
