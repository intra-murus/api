package com.s6info01.intramurusapi.business.types;

public class Etudiant {
    private String app;
    private String cip_etudiant;
    private String cote_r;
    private String courriel;
    private String departement;
    private String inscription;
    private String nom;
    private String prenom;
    private String profil_id;
    private String programme;
    private String trimestre_id;
    private String unit_id;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getCip() {
        return cip_etudiant;
    }

    public void setCip_etudiant(String cip_etudiant) {
        this.cip_etudiant = cip_etudiant;
    }

    public String getCote() {
        return cote_r;
    }

    public void setCote_r(String cote_r) {
        this.cote_r = cote_r;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getInscription() {
        return inscription;
    }

    public void setInscription(String inscription) {
        this.inscription = inscription;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getProfilId() {
        return profil_id;
    }

    public void setProfil_id(String profil_id) {
        this.profil_id = profil_id;
    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public String getTrimestreId() {
        return trimestre_id;
    }

    public void setTrimestre_id(String trimestre_id) {
        this.trimestre_id = trimestre_id;
    }

    public String getUnitId() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }
}
