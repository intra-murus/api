package com.s6info01.intramurusapi.business.types;


public class FiltresView {

  private EPeriode periode;
  private long annee;
  private String sport;
  private String nomLigue;


  public EPeriode getPeriode() {
    return periode;
  }

  public void setPeriode(EPeriode periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public String getNomLigue() {
    return nomLigue;
  }

  public void setNomLigue(String nomLigue) {
    this.nomLigue = nomLigue;
  }

}
