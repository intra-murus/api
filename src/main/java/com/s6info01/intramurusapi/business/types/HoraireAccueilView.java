package com.s6info01.intramurusapi.business.types;


public class HoraireAccueilView {

    private EStatutMembreMatch statutMembreMatch;
    private long idMatch;
    private String terrain;
    private EStatutMatch statutMatch;
    private java.sql.Timestamp dateDebut;
    private java.sql.Timestamp dateFin;
    private String sport;
    private String nomLigue;
    private String nomEquipe;

    public EStatutMembreMatch getStatutMembreMatch() {
        return statutMembreMatch;
    }

    public void setStatutMembreMatch(EStatutMembreMatch statutMembreMatch) {
        this.statutMembreMatch = statutMembreMatch;
    }


    public long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(long idMatch) {
        this.idMatch = idMatch;
    }


    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }


    public EStatutMatch getStatutMatch() {
        return statutMatch;
    }

    public void setStatutMatch(EStatutMatch statutMatch) {
        this.statutMatch = statutMatch;
    }


    public java.sql.Timestamp getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(java.sql.Timestamp dateDebut) {
        this.dateDebut = dateDebut;
    }


    public java.sql.Timestamp getDateFin() {
        return dateFin;
    }

    public void setDateFin(java.sql.Timestamp dateFin) {
        this.dateFin = dateFin;
    }


    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }


    public String getNomLigue() {
        return nomLigue;
    }

    public void setNomLigue(String nomLigue) {
        this.nomLigue = nomLigue;
    }


    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

}
