package com.s6info01.intramurusapi.business.types;


public class Ligue {

  private String sport;
  private EPeriode periode;
  private long annee;
  private String nomLigue;
  private long nbrMinEquipe;
  private long nbrMaxEquipe;


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public EPeriode getPeriode() {
    return periode;
  }

  public void setPeriode(EPeriode periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public String getNomLigue() {
    return nomLigue;
  }

  public void setNomLigue(String nomLigue) {
    this.nomLigue = nomLigue;
  }


  public long getNbrMinEquipe() {
    return nbrMinEquipe;
  }

  public void setNbrMinEquipe(long nbrMinEquipe) {
    this.nbrMinEquipe = nbrMinEquipe;
  }


  public long getNbrMaxEquipe() {
    return nbrMaxEquipe;
  }

  public void setNbrMaxEquipe(long nbrMaxEquipe) {
    this.nbrMaxEquipe = nbrMaxEquipe;
  }

}
