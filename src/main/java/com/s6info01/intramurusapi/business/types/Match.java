package com.s6info01.intramurusapi.business.types;


public class Match {

  private long idMatch;
  private String terrain;
  private EStatutMatch statutMatch;
  private String sport;
  private EPeriode periode;
  private long annee;
  private String nomLigue;
  private String cip;
  private java.sql.Timestamp dateDebut;
  private java.sql.Timestamp dateFin;


  public long getIdMatch() {
    return idMatch;
  }

  public void setIdMatch(long idMatch) {
    this.idMatch = idMatch;
  }


  public String getTerrain() {
    return terrain;
  }

  public void setTerrain(String terrain) {
    this.terrain = terrain;
  }


  public EStatutMatch getStatutMatch() {
    return statutMatch;
  }

  public void setStatutMatch(EStatutMatch statutMatch) {
    this.statutMatch = statutMatch;
  }


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public EPeriode getPeriode() {
    return periode;
  }

  public void setPeriode(EPeriode periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public String getNomLigue() {
    return nomLigue;
  }

  public void setNomLigue(String nomLigue) {
    this.nomLigue = nomLigue;
  }


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }


  public java.sql.Timestamp getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(java.sql.Timestamp dateDebut) {
    this.dateDebut = dateDebut;
  }


  public java.sql.Timestamp getDateFin() {
    return dateFin;
  }

  public void setDateFin(java.sql.Timestamp dateFin) {
    this.dateFin = dateFin;
  }

}
