package com.s6info01.intramurusapi.business.types;


public class MatchView {

  private long idMatch;
  private String statutMatch;
  private String sport;
  private String nomLigue;
  private String periode;
  private long annee;
  private java.sql.Timestamp dateDebut;
  private java.sql.Timestamp dateFin;
  private String terrain;
  private String arbitre;
  private long idEquipe;
  private String nomEquipe;
  private String cipCapitaine;
  private EStatutEquipeMatch statutEquipeMatch;
  private String cote;


  public long getIdMatch() {
    return idMatch;
  }

  public void setIdMatch(long idMatch) {
    this.idMatch = idMatch;
  }


  public String getStatutMatch() {
    return statutMatch;
  }

  public void setStatutMatch(String statutMatch) {
    this.statutMatch = statutMatch;
  }


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public String getNomLigue() {
    return nomLigue;
  }

  public void setNomLigue(String nomLigue) {
    this.nomLigue = nomLigue;
  }


  public String getPeriode() {
    return periode;
  }

  public void setPeriode(String periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public java.sql.Timestamp getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(java.sql.Timestamp dateDebut) {
    this.dateDebut = dateDebut;
  }


  public java.sql.Timestamp getDateFin() {
    return dateFin;
  }

  public void setDateFin(java.sql.Timestamp dateFin) {
    this.dateFin = dateFin;
  }


  public String getTerrain() {
    return terrain;
  }

  public void setTerrain(String terrain) {
    this.terrain = terrain;
  }


  public String getArbitre() {
    return arbitre;
  }

  public void setArbitre(String arbitre) {
    this.arbitre = arbitre;
  }


  public long getIdEquipe() {
    return idEquipe;
  }

  public void setIdEquipe(long idEquipe) {
    this.idEquipe = idEquipe;
  }


  public String getNomEquipe() {
    return nomEquipe;
  }

  public void setNomEquipe(String nomEquipe) {
    this.nomEquipe = nomEquipe;
  }


  public String getCipCapitaine() {
    return cipCapitaine;
  }

  public void setCipCapitaine(String cipCapitaine) {
    this.cipCapitaine = cipCapitaine;
  }


  public EStatutEquipeMatch getStatutEquipeMatch() {
    return statutEquipeMatch;
  }

  public void setStatutEquipeMatch(EStatutEquipeMatch statutEquipeMatch) {
    this.statutEquipeMatch = statutEquipeMatch;
  }


  public String getCote() {
    return cote;
  }

  public void setCote(String cote) {
    this.cote = cote;
  }

}
