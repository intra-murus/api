package com.s6info01.intramurusapi.business.types;

public class Membre {

  private String cip;
  private String nom;
  private String prenom;
  private String email;


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "{" +
            "\"cip\":\"" + cip + "\"," +
            "\"nom\":\"" + nom + "\"," +
            "\"prenom\":\"" + prenom + "\"," +
            "\"email\":\"" + email + "\"" +
            "}";
  }
}
