package com.s6info01.intramurusapi.business.types;


public class MembreEquipe {

    private long idEquipe;
    private String cip;
    private EStatutMembreEquipe statutMembreEquipe;


    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }


    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }


    public EStatutMembreEquipe getStatutMembreEquipe() {
        return statutMembreEquipe;
    }

    public void setStatutMembreEquipe(EStatutMembreEquipe statutMembreEquipe) {
        this.statutMembreEquipe = statutMembreEquipe;
    }

    @Override
    public String toString() {
        return "{" +
                "cip='" + cip + '\'' +
                ", idEquipe=" + idEquipe +
                ", statutMembreEquipe=" + statutMembreEquipe +
                '}';
    }
}
