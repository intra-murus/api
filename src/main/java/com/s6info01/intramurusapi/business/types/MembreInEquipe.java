package com.s6info01.intramurusapi.business.types;


public class MembreInEquipe {

    private String cip;
    private String nom;
    private String prenom;
    private EStatutMembreEquipe statutMembreEquipe;

    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public EStatutMembreEquipe getStatutMembreEquipe() {
        return statutMembreEquipe;
    }

    public void setStatutMembreEquipe(EStatutMembreEquipe statutMembreEquipe) {
        this.statutMembreEquipe = statutMembreEquipe;
    }
}
