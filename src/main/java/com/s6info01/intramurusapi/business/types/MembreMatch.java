package com.s6info01.intramurusapi.business.types;


public class MembreMatch {

  private String cip;
  private long idMatch;
  private String statutMembre;


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }


  public long getIdMatch() {
    return idMatch;
  }

  public void setIdMatch(long idMatch) {
    this.idMatch = idMatch;
  }


  public String getStatutMembre() {
    return statutMembre;
  }

  public void setStatutMembre(String statutMembre) {
    this.statutMembre = statutMembre;
  }

}
