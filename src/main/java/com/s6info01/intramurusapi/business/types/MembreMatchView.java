package com.s6info01.intramurusapi.business.types;


public class MembreMatchView {

    private String cip;
    private String prenom;
    private String nom;
    private EStatutMembreMatch statutMembreMatch;
    private long idEquipe;
    private long idMatch;


    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }


    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public EStatutMembreMatch getStatutMembreMatch() {
        return statutMembreMatch;
    }

    public void setStatutMembreMatch(EStatutMembreMatch statutMembreMatch) {
        this.statutMembreMatch = statutMembreMatch;
    }


    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }


    public long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(long idMatch) {
        this.idMatch = idMatch;
    }

}
