package com.s6info01.intramurusapi.business.types;


public class MembreMessage {

  private long idMessage;
  private String cip;


  public long getIdMessage() {
    return idMessage;
  }

  public void setIdMessage(long idMessage) {
    this.idMessage = idMessage;
  }


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }

}
