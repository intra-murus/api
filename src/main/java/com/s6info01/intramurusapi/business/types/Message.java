package com.s6info01.intramurusapi.business.types;


public class Message {

    private long idMessage;
    private ETypeMessage typeMessage;
    private String text;
    private Long lien;


    public long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }


    public ETypeMessage getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(ETypeMessage typeMessage) {
        this.typeMessage = typeMessage;
    }


    public String getText() {
        return text;
    }

    public void setText(String message) {
        this.text = message;
    }

    public Long getLien() {
        return lien;
    }

    public void setLien(Long lien) {
        this.lien = lien;
    }

    @Override
    public String toString() {
        return "{" +
                (idMessage == 0 ? "" : "idMessage=" + idMessage) +
                "typeMessage=" + typeMessage +
                ", text='" + text + '\'' +
                ", lien=" + lien +
                '}';
    }
}
