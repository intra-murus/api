package com.s6info01.intramurusapi.business.types;


public class MessageView {

  private long idMessage;
  private String typeMessage;
  private String text;
  private long lien;

  public long getIdMessage() {
    return idMessage;
  }

  public void setIdMessage(long idMessage) {
    this.idMessage = idMessage;
  }


  public String getTypeMessage() {
    return typeMessage;
  }

  public void setTypeMessage(String typeMessage) {
    this.typeMessage = typeMessage;
  }


  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }


  public long getLien() {
    return lien;
  }

  public void setLien(long lien) {
    this.lien = lien;
  }

}
