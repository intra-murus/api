package com.s6info01.intramurusapi.business.types;


public class PeriodeSport {

  private String sport;
  private String periode;


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public String getPeriode() {
    return periode;
  }

  public void setPeriode(String periode) {
    this.periode = periode;
  }

}
