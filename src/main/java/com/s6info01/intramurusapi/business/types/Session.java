package com.s6info01.intramurusapi.business.types;


public class Session {

  private String periode;
  private long annee;
  private java.sql.Date dateLimiteInscription;


  public String getPeriode() {
    return periode;
  }

  public void setPeriode(String periode) {
    this.periode = periode;
  }


  public long getAnnee() {
    return annee;
  }

  public void setAnnee(long annee) {
    this.annee = annee;
  }


  public java.sql.Date getDateLimiteInscription() {
    return dateLimiteInscription;
  }

  public void setDateLimiteInscription(java.sql.Date dateLimiteInscription) {
    this.dateLimiteInscription = dateLimiteInscription;
  }

}
