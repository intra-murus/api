package com.s6info01.intramurusapi.business.types;


public class SportTerrainDisponibilite {

  private String terrain;
  private String sport;
  private String jour;
  private java.sql.Time heureDebut;
  private java.sql.Time heureFin;


  public String getTerrain() {
    return terrain;
  }

  public void setTerrain(String terrain) {
    this.terrain = terrain;
  }


  public String getSport() {
    return sport;
  }

  public void setSport(String sport) {
    this.sport = sport;
  }


  public String getJour() {
    return jour;
  }

  public void setJour(String jour) {
    this.jour = jour;
  }


  public java.sql.Time getHeureDebut() {
    return heureDebut;
  }

  public void setHeureDebut(java.sql.Time heureDebut) {
    this.heureDebut = heureDebut;
  }


  public java.sql.Time getHeureFin() {
    return heureFin;
  }

  public void setHeureFin(java.sql.Time heureFin) {
    this.heureFin = heureFin;
  }

}
