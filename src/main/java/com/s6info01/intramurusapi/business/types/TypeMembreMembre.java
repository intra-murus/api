package com.s6info01.intramurusapi.business.types;


public class TypeMembreMembre {

  private String typeMembre;
  private String cip;


  public String getTypeMembre() {
    return typeMembre;
  }

  public void setTypeMembre(String typeMembre) {
    this.typeMembre = typeMembre;
  }


  public String getCip() {
    return cip;
  }

  public void setCip(String cip) {
    this.cip = cip;
  }

}
