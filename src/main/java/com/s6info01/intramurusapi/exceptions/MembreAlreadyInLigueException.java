package com.s6info01.intramurusapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MembreAlreadyInLigueException extends RuntimeException {

    public MembreAlreadyInLigueException(String exception) {
        super(exception);
    }
}