package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.Equipe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface EquipeMapper {

    Equipe findById(final Long id_equipe);

    List<Equipe> findByCip(final String cip);

    int insert(final Equipe equipe);

    int update(final Equipe equipe);

    int deleteById(@Param("id_equipe") final Long id_equipe);

}
