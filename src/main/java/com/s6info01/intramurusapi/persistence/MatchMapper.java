package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.Match;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface MatchMapper {

    Match findById(@Param("idMatch") final Long idMatch);

    int insert(final Match match);

    int update(final Match match);

    int deleteById(@Param("idMatch") final Long idMatch);
}
