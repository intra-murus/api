package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.MembreEquipe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface MembreEquipeMapper {

    MembreEquipe findOne(@Param("cip") final String cip, @Param("id_equipe") final Long id_equipe);

    int insert(final MembreEquipe membreEquipe);

    int update(final MembreEquipe membreEquipe);

    int deleteOne(final String cip, final Long id_equipe);

}
