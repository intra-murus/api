package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.Membre;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MembreMapper {

    @Results({
        @Result(property = "cip", column = "cip"),
        @Result(property = "nom", column = "nom"),
        @Result(property = "prenom", column = "prenom"),
        @Result(property = "email", column = "email")
    })
    @Select("SELECT * FROM membre WHERE cip = #{cip}")
    Membre findOne(@Param("cip") final String cip);

    @Results({
            @Result(property = "cip", column = "cip"),
            @Result(property = "nom", column = "nom"),
            @Result(property = "prenom", column = "prenom"),
            @Result(property = "email", column = "email")
    })
    @Select("SELECT * FROM membre")
    List<Membre> findAll();

    @Insert("INSERT INTO membre(cip,nom,prenom,email) VALUES(#{cip},#{nom},#{prenom},#{email})")
    void insert(@Param("cip") final String cip,
                @Param("nom") final String nom,
                @Param("prenom") final String prenom,
                @Param("email") final String email);

    @Update("UPDATE membre SET nom=#{nom}, prenom=#{prenom}, email=#{email} WHERE cip = #{cip}")
    void update(@Param("cip") final String cip,
                @Param("nom") final String nom,
                @Param("prenom") final String prenom,
                @Param("email") final String email);

    @Delete("DELETE FROM membre WHERE cip = #{cip}")
    void delete(@Param("cip") final String cip);
}
