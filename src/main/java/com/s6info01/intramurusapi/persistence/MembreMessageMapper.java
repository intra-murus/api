package com.s6info01.intramurusapi.persistence;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MembreMessageMapper {

    @Results({
            @Result(property = "idMessage", column = "id_message")
    })
    @Select("SELECT id_message FROM membre_message WHERE cip = #{cip}")
    List<Integer> findAll(@Param("cip") final String cip);

    @Insert("INSERT INTO membre_message(cip, id_message) VALUES(#{cip},#{id_message})")
    int insert(@Param("cip") final String cip,
               @Param("id_message") final Long id_message);

    @Delete("DELETE FROM membre_message WHERE cip = #{cip} AND id_message = #{id_message}")
    void delete(@Param("cip") final String cip,
                @Param("id_message") final Long id_message);
}
