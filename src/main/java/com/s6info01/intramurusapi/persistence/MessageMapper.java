package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.Message;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MessageMapper {

    @Results(id = "messageResult", value = {
            @Result(property = "idMessage", column = "id_message"),
            @Result(property = "typeMessage", column = "type_message"),
            @Result(property = "text", column = "text"),
            @Result(property = "lien", column = "lien")
    })
    @Select("SELECT * FROM messages WHERE id_message = #{id_message}")
    Message findOne(@Param("id_message") final Integer id_message);

    @ResultMap("messageResult")
    @Select("SELECT * FROM messages")
    List<Message> findAll();

    @Insert("INSERT INTO messages(type_message,text,lien) VALUES(#{message.typeMessage},#{message.text},#{message.lien})")
    @Options(keyProperty = "message.idMessage", keyColumn = "id_message", useGeneratedKeys = true)
    int insert(@Param("message") final Message message);

    @Update("UPDATE messages SET text=#{text} WHERE id_message = #{id_message}")
    void update(@Param("id_message") final Integer id_message,
                @Param("text") final String text);

    @Delete("DELETE FROM messages WHERE id_message = #{id_message}")
    void delete(@Param("id_message") final Integer id_message);
}
