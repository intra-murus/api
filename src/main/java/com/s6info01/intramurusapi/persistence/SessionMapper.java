package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.EPeriode;
import com.s6info01.intramurusapi.business.types.Session;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
@Mapper
public interface SessionMapper {
    @Results({
            @Result(property = "periode", column = "periode"),
            @Result(property = "annee", column = "annee"),
            @Result(property = "dateLimiteInscription", column = "date_limite_inscription")
    })
    @Select("SELECT * FROM session WHERE periode = #{periode} AND annee = #{annee}")
    Session findOne(@Param("periode") final EPeriode periode,
                    @Param("annee") final Integer annee);

    @Results({
            @Result(property = "periode", column = "periode"),
            @Result(property = "annee", column = "annee"),
            @Result(property = "dateLimiteInscription", column = "date_limite_inscription")
    })
    @Select("SELECT * FROM session")
    List<Session> findAll();

    @Insert("INSERT INTO session(periode, annee, date_limite_inscription) " +
            "VALUES(#{periode},#{annee},#{date_limite_inscription})")
    void insert(@Param("periode") final EPeriode periode,
                @Param("annee") final Integer annee,
                @Param("date_limite_inscription") final Date date_limite_inscription);

    @Update("UPDATE session SET periode=#{periode}, annee=#{annee}, date_limite_inscription=#{date_limite_inscription}")
    void update(@Param("periode") final EPeriode periode,
                @Param("annee") final Integer annee,
                @Param("date_limite_inscription") final Date date_limite_inscription);

    @Delete("DELETE FROM session WHERE periode = #{periode} AND annee = #{annee}")
    void delete(@Param("periode") final EPeriode periode,
                @Param("annee") final Integer annee);
}
