package com.s6info01.intramurusapi.persistence;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TerrainMapper {

    @Results({
            @Result(property = "terrain", column = "terrain")
    })
    @Select("SELECT * FROM terrain")
    List<String> findAll();

    @Insert("INSERT INTO terrain(terrain) VALUES(#{nom_terrain})")
    void insert(@Param("nom_terrain") final String nom_terrain);

    @Update("UPDATE terrain SET terrain=#{new_terrain} WHERE terrain = #{old_terrain}")
    void update(@Param("old_terrain") final String old_terrain,
                @Param("new_terrain") final String new_terrain);

    @Delete("DELETE FROM terrain WHERE terrain = #{nom_terrain}")
    void delete(@Param("nom_terrain") final String nom_terrain);
}
