package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.ETypeDeMembre;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TypeMembreMembreMapper {

    @Results({
            @Result(property = "typeMembre", column = "type_membre")
    })
    @Select("SELECT type_membre FROM type_membre_membre WHERE cip = #{cip}")
    List<ETypeDeMembre> findAll(@Param("cip") final String cip);

    @Insert("INSERT INTO type_membre_membre(cip,type_membre) VALUES(#{cip},#{type_membre})")
    void insert(@Param("cip") final String cip,
                @Param("type_membre") final ETypeDeMembre typeDeMembre);

    @Update("UPDATE type_membre_membre SET cip=#{cip}, type_membre=#{new_type_membre} WHERE cip = #{cip} AND type_membre = #{type_membre}")
    void update(@Param("cip") final String cip,
                @Param("type_membre") final ETypeDeMembre typeDeMembre,
                @Param("new_type_membre") final ETypeDeMembre newTypeDeMembre);

    @Delete("DELETE FROM type_membre_membre WHERE cip = #{cip} AND type_membre = #{type_membre}")
    void delete(@Param("cip") final String cip,
                @Param("type_membre") final ETypeDeMembre typeDeMembre);
}
