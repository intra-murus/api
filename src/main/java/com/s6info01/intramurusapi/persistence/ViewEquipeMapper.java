package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.EPeriode;
import com.s6info01.intramurusapi.business.types.EStatutApprobation;
import com.s6info01.intramurusapi.business.types.EquipeView;
import com.s6info01.intramurusapi.business.types.MembreInEquipe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ViewEquipeMapper {

    List<EquipeView> findByFilters(@Param("periode") final EPeriode periode,
                                   @Param("annee") final Integer annee,
                                   @Param("sport") final String sport,
                                   @Param("nomLigue") final String nomLigue,
                                   @Param("statutApprobation") final EStatutApprobation statutApprobation);

    List<EquipeView> findAllSports(@Param("periode") final EPeriode periode,
                                   @Param("annee") final Integer annee,
                                   @Param("statutApprobation") final EStatutApprobation statutApprobation);

    List<EquipeView> findAllLigues(@Param("periode") final EPeriode periode,
                                   @Param("annee") final Integer annee,
                                   @Param("sport") final String sport,
                                   @Param("statutApprobation") final EStatutApprobation statutApprobation);

    List<Long> getEquipeInLigueForMembre(@Param("cip") final String cip,
                                         @Param("periode") final EPeriode periode,
                                         @Param("annee") final Integer annee,
                                         @Param("sport") final String sport,
                                         @Param("nomLigue") final String nomLigue);

    List<String> getCipsInEquipe(@Param("idEquipe") final Long idEquipe);

    List<MembreInEquipe> getMembresEquipeInfo(@Param("idEquipe") final Long idEquipe);

    List<Integer> getDistinctIdEquipes(@Param("periode") final EPeriode periode,
                                       @Param("annee") final Integer annee,
                                       @Param("sport") final String sport,
                                       @Param("nomLigue") final String nomLigue,
                                       @Param("statutApprobation") final EStatutApprobation statutApprobation);

    List<Integer> getDistinctIdEquipesAllSports(@Param("periode") final EPeriode periode,
                                                @Param("annee") final Integer annee,
                                                @Param("statutApprobation") final EStatutApprobation statutApprobation);

    List<Integer> getDistinctIdEquipesAllLigues(@Param("periode") final EPeriode periode,
                                                @Param("annee") final Integer annee,
                                                @Param("sport") final String sport,
                                                @Param("statutApprobation") final EStatutApprobation statutApprobation);

    Integer countMembersInEquipe(@Param("idEquipe") final Integer idEquipe);

}
