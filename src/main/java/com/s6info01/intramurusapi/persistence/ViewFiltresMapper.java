package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.EPeriode;
import com.s6info01.intramurusapi.business.types.FiltresView;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ViewFiltresMapper {

    List<Integer> findDistinctAnnees();

    List<String> findDistinctPeriodes();

    List<String> findDistinctSports(@Param("annee") final Integer annee,
                                    @Param("periode") final EPeriode periode);

    List<String> findDistinctLigues(@Param("annee") final Integer annee,
                                    @Param("periode") final EPeriode periode,
                                    @Param("sport") final String sport);
}
