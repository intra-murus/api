package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.HoraireAccueilView;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
@Mapper
public interface ViewHoraireAccueilMapper {

    List<HoraireAccueilView> findByCip(@Param("cip") final String cip,
                                       @Param("dateDebut") final long dateDebut,
                                       @Param("dateFin") final long dateFin);
}
