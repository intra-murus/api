package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.MatchView;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ViewMatchMapper {

    List<MatchView> findByMatchId(@Param("idMatch") final Long idMatch);

    MatchView findByEquipeIdAndMatchId(@Param("idEquipe") final Long idEquipe,
                                       @Param("idMatch") final Long idMatch);

    List<MatchView> findMatchsByTerrain(@Param("terrain") final String terrain,
                                        @Param("dateDebut") final long dateDebut,
                                        @Param("dateFin") final long dateFin);

    int updateStatutEquipeMatch(MatchView matchView);

    int insertIntoEquipeMatch(MatchView matchView);

    int deleteFromEquipeMatch(@Param("idEquipe") final Long idEquipe, @Param("idMatch") final Long idMatch);

}
