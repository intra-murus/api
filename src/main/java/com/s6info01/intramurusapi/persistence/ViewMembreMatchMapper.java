package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.MembreMatchView;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ViewMembreMatchMapper {

    List<MembreMatchView> findByMatchId(@Param("idMatch") final Long idMatch);

    MembreMatchView findByCipAndMatchId(@Param("idMatch") final Long idMatch,
                                        @Param("cip") final String cip);

    int updateStatutMembreMatch(final MembreMatchView membreMatchView);

    int insertIntoMembreMatch(final MembreMatchView membreMatchView);

    int deleteFromMembreMatch(@Param("cip") final String cip, @Param("idMatch") final Long idMatch);

}
