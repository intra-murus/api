package com.s6info01.intramurusapi.persistence;

import com.s6info01.intramurusapi.business.types.MessageView;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ViewMessageMapper {

    List<MessageView> findByCip(@Param("cip") final String cip);

}
