package com.s6info01.intramurusapi.presentation;

import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.service.EquipeService;
import com.s6info01.intramurusapi.business.service.MessageService;
import com.s6info01.intramurusapi.business.types.*;
import com.s6info01.intramurusapi.presentation.requestbody.CreateEquipeRequestBody;
import com.s6info01.intramurusapi.presentation.requestbody.UpdateStatutApprobationRequestBody;
import com.s6info01.intramurusapi.presentation.requestbody.UpdateStatutMembreEquipeRequestBody;
import com.s6info01.intramurusapi.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/equipes")
public class EquipeController {

    private final EquipeService equipeService;
    private final MessageService messageService;

    @Autowired
    public EquipeController(final EquipeService equipeService,
                            final MessageService messageService) {
        this.equipeService = equipeService;
        this.messageService = messageService;
    }

    @GetMapping("/getEquipeById")
    public Equipe getEquipeById(@RequestParam("id_equipe") final Long id) {
        return equipeService.getEquipeById(id);
    }

    @GetMapping("/getEquipesForMembre")
    public List<Equipe> getEquipesForMembre(@RequestParam(required = false) final String cip,
                                            final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(cip) ? user.getCip() : cip;
        return equipeService.getEquipesByCip(cipToUse);
    }

    @GetMapping("/getMembresInEquipe")
    public List<MembreInEquipe> getMembresInEquipe(final Long idEquipe) {
        return equipeService.getMembresEquipeInfo(idEquipe);
    }

    @GetMapping("/getEquipeApprobationView")
    public List<EquipeView> getEquipeApprobationView(@RequestParam(required = false) final String sport,
                                                     @RequestParam(required = false) final String nomLigue,
                                                     @RequestParam final EStatutApprobation statutApprobation) {
        return equipeService.getApprobationView(SessionUtil.getCurrentPeriode(),
                LocalDate.now().getYear(),
                sport,
                nomLigue,
                statutApprobation);
    }

    @PutMapping("/updateStatutApprobation")
    public void updateStatutApprobation(@RequestBody final UpdateStatutApprobationRequestBody request) {
        equipeService.updateStatutAppbrobation(request.getIdEquipe(), request.getStatutApprobation());
    }

    @PutMapping("/updateStatutMembreEquipe")
    public void updateStatutMembreEquipe(@RequestBody final UpdateStatutMembreEquipeRequestBody updateRequest,
                                         final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(updateRequest.getCip()) ? user.getCip() : updateRequest.getCip();
        equipeService.updateStatutMembreEquipe(cipToUse, updateRequest.getIdEquipe(), updateRequest.getStatutMembreEquipe());
    }

    @PostMapping("/createEquipe")
    public void createEquipe(@RequestBody final CreateEquipeRequestBody createEquipeRequest,
                             final HttpServletRequest httpRequest) {

        final List<String> cips = new ArrayList<>(Arrays.asList(createEquipeRequest.getCipsJoueurs().split(",")));

        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(createEquipeRequest.getCipCapitaine()) ? user.getCip() : createEquipeRequest.getCipCapitaine();

        final Equipe equipe = equipeService.createEquipe(createEquipeRequest.getSport(),
                createEquipeRequest.getNomLigue(),
                createEquipeRequest.getNomEquipe(),
                cipToUse,
                cips);

        final List<Membre> joueurs = equipeService.getMembresInEquipe(equipe.getIdEquipe());
        messageService.createInvitationEquipe(equipe, joueurs);
    }
}
