package com.s6info01.intramurusapi.presentation;

import com.s6info01.intramurusapi.business.types.EPeriode;
import com.s6info01.intramurusapi.persistence.ViewFiltresMapper;
import com.s6info01.intramurusapi.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/filtres")
public class FiltresViewController {
    private final ViewFiltresMapper viewFiltresMapper;

    @Autowired
    public FiltresViewController(final ViewFiltresMapper viewFiltresMapper) {
        this.viewFiltresMapper = viewFiltresMapper;
    }

    @GetMapping("/sports")
    @ResponseBody
    public List<String> getSports(@RequestParam(required = false) Integer annee,
                                  @RequestParam(required = false) EPeriode periode) {
        if(annee == null && periode == null) {
            periode = SessionUtil.getCurrentPeriode();
            annee = LocalDate.now().getYear();
        }

        return viewFiltresMapper.findDistinctSports(annee, periode);
    }

    @GetMapping("/ligues")
    @ResponseBody
    public List<String> getLigues(@RequestParam(required = false) Integer annee,
                                  @RequestParam(required = false) EPeriode periode,
                                  @RequestParam final String sport) {
        if(annee == null && periode == null) {
            periode = SessionUtil.getCurrentPeriode();
            annee = LocalDate.now().getYear();
        }

        return viewFiltresMapper.findDistinctLigues(annee, periode, sport);
    }

}
