package com.s6info01.intramurusapi.presentation;

import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.types.HoraireAccueilView;
import com.s6info01.intramurusapi.business.types.Membre;
import com.s6info01.intramurusapi.persistence.ViewHoraireAccueilMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/api/horaire")
public class HoraireController {

    private final ViewHoraireAccueilMapper viewHoraireAccueilMapper;

    @Autowired
    public HoraireController(final ViewHoraireAccueilMapper viewHoraireAccueilMapper) {
        this.viewHoraireAccueilMapper = viewHoraireAccueilMapper;
    }

    @GetMapping
    @ResponseBody
    public List<HoraireAccueilView> getHoraire(@RequestParam(required = false) final String cip,
                                               @RequestParam final long startDate,
                                               @RequestParam final long endDate,
                                               final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(cip) ? user.getCip() : cip;
        return viewHoraireAccueilMapper.findByCip(cipToUse, startDate, endDate);
    }
}
