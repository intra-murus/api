package com.s6info01.intramurusapi.presentation;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.service.EtudiantService;
import com.s6info01.intramurusapi.business.types.ETypeDeMembre;
import com.s6info01.intramurusapi.business.types.Etudiant;
import com.s6info01.intramurusapi.business.types.Membre;
import com.s6info01.intramurusapi.persistence.MembreMapper;
import com.s6info01.intramurusapi.persistence.TypeMembreMembreMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@RestController
@RequestMapping("/login")
public class LoginController {

    private final MembreMapper membreMapper;
    private final TypeMembreMembreMapper typeMembreMembreMapper;
    private final EtudiantService etudiantService;

    @Autowired
    public LoginController(final MembreMapper membreMapper,
                           final TypeMembreMembreMapper typeMembreMembreMapper,
                           final EtudiantService etudiantService) {
        this.membreMapper = membreMapper;
        this.typeMembreMembreMapper = typeMembreMembreMapper;
        this.etudiantService = etudiantService;
    }

    @GetMapping
    @ResponseBody
    public String login(final HttpServletRequest request,
                        final HttpServletResponse response,
                        @RequestParam(required = false) String backurl) throws IOException {
        Principal principal = request.getUserPrincipal();

        Membre membre = membreMapper.findOne(principal.getName());
        if(membre == null) {
            Etudiant etudiant = etudiantService.getEtudiantInfo(principal.getName());
            membre = new Membre();
            membre.setCip(etudiant.getCip());
            membre.setNom(etudiant.getNom());
            membre.setPrenom(etudiant.getPrenom());
            membre.setEmail(etudiant.getCourriel());
            membreMapper.insert(membre.getCip(), membre.getNom(), membre.getPrenom(), membre.getEmail());

            typeMembreMembreMapper.insert(membre.getCip(), ETypeDeMembre.JOUEUR);
        }

        String token = "";
        try {
            Algorithm algorithm = Algorithm.HMAC256("C4r1osM4tos420");
            token = JWT.create()
                    .withClaim("user", membre.toString())
                    .withExpiresAt(Date.from(LocalDate.now()
                            .plusMonths(4)
                            .atStartOfDay()
                            .atZone(ZoneId.systemDefault())
                            .toInstant()))
                    .sign(algorithm);
        } catch (JWTCreationException e){
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Could not create token");
        }

        if(Strings.isNullOrEmpty(backurl)) {
            return new JSONObject().put("token", token).toString();
        } else {
            URI uri = UriBuilder.fromUri(backurl).queryParam("jwt", token).build();
            response.sendRedirect(uri.toString());
            return "";
        }
    }
}
