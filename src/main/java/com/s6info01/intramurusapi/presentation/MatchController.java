package com.s6info01.intramurusapi.presentation;

import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.service.EquipeService;
import com.s6info01.intramurusapi.business.service.MatchService;
import com.s6info01.intramurusapi.business.types.Match;
import com.s6info01.intramurusapi.business.types.MatchView;
import com.s6info01.intramurusapi.business.types.Membre;
import com.s6info01.intramurusapi.business.types.MembreMatchView;
import com.s6info01.intramurusapi.presentation.requestbody.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/matchs")
public class MatchController {

    private final MatchService matchService;
    private final EquipeService equipeService;

    public MatchController(final MatchService matchService, final EquipeService equipeService) {
        this.matchService = matchService;
        this.equipeService = equipeService;
    }

    @GetMapping("/getEquipesForMatch")
    public List<MatchView> getEquipesForMatch(final long matchId) {
        return matchService.getEquipeMatch(matchId);
    }

    @GetMapping("/getMembresForMatch")
    public List<MembreMatchView> getMembresForMatch(final long matchId) {
        return matchService.getMembreMatch(matchId);
    }

    @GetMapping("/getMatchsByTerrain")
    public List<MatchView> getMatchsByTerrain(@RequestParam final String terrain,
                                              @RequestParam final long startDate,
                                              @RequestParam final long endDate) {
        return matchService.getMatchsByTerrain(terrain,
                startDate,
                endDate);
    }

    @GetMapping("/getMatch")
    public Match getMatch(final Long matchId) {
        return matchService.getMatchById(matchId);
    }

    @PutMapping("/updateStatutMembreMatch")
    public void updateStatutMembreMatch(@RequestBody final UpdateStatutMembreMatchRequestBody updateRequest,
                                        final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(updateRequest.getCip()) ? user.getCip() : updateRequest.getCip();
        matchService.updateStatutMembreMatch(updateRequest.getIdMatch(), cipToUse, updateRequest.getStatutMembreMatch());
    }

    @PutMapping("/updateStatutEquipeMatch")
    public void updateStatutEquipeMatch(@RequestBody final UpdateStatutEquipeMatchRequestBody updateRequest) {
        matchService.updateStatutEquipeMatch(updateRequest.getIdEquipe(), updateRequest.getIdMatch(), updateRequest.getStatutEquipeMatch());
    }

    @PutMapping("/updateMatch")
    public void updateMatch(@RequestBody final UpdateMatchRequestBody updateRequest) {
        matchService.updateMatch(updateRequest.getIdMatch(),
                updateRequest.getDateDebut(),
                updateRequest.getDateFin());
    }

    @PostMapping("/createMatch")
    public Match createMatch(@RequestBody final CreateMatchRequestBody createRequest) {
        List<Membre> membresMatch = equipeService.getMembresInEquipe(createRequest.getIdEquipeHome());
        membresMatch.addAll(equipeService.getMembresInEquipe(createRequest.getIdEquipeAway()));

        return matchService.insertMatch(createRequest.getIdEquipeHome(), createRequest.getIdEquipeAway(), createRequest.getDateDebut(),
                                 createRequest.getDateFin(), createRequest.getTerrain(), createRequest.getSport(),
                                 createRequest.getNomLigue(), createRequest.getArbitre(), membresMatch);

    }

    @DeleteMapping("/deleteMatch")
    public void deleteMatch(@RequestParam final Long idMatch) {
        matchService.deleteMatch(idMatch);
    }
}
