package com.s6info01.intramurusapi.presentation;

import com.google.common.base.Strings;
import com.s6info01.intramurusapi.business.service.EquipeService;
import com.s6info01.intramurusapi.business.service.MessageService;
import com.s6info01.intramurusapi.business.types.Membre;
import com.s6info01.intramurusapi.business.types.MessageView;
import com.s6info01.intramurusapi.presentation.requestbody.CreateMessageRequestBody;
import com.s6info01.intramurusapi.presentation.requestbody.DeleteMembreMessageRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(final MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/getMessagesForMembre")
    public List<MessageView> getMessagesForMembre(@RequestParam(required = false) final String cip,
                                                  final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(cip) ? user.getCip() : cip;
        return messageService.getMessagesByCip(cipToUse);
    }

    @PostMapping("/createMessage")
    public void createMessage(@RequestBody final CreateMessageRequestBody requestBody,
                                           final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(requestBody.getCipCapitaine()) ? user.getCip() : requestBody.getCipCapitaine();
        messageService.createMessageToEquipe(cipToUse, requestBody.getIdEquipe(), requestBody.getMessage());
    }

    @DeleteMapping("/deleteMessage")
    public void createMessage(@RequestBody final DeleteMembreMessageRequestBody requestBody,
                              final HttpServletRequest httpRequest) {
        final Membre user = (Membre) httpRequest.getAttribute("user");
        final String cipToUse = Strings.isNullOrEmpty(requestBody.getCip()) ? user.getCip() : requestBody.getCip();
        messageService.deleteMembreMessage(cipToUse, requestBody.getMessageId());
    }
}
