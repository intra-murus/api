package com.s6info01.intramurusapi.presentation;

import com.s6info01.intramurusapi.persistence.TerrainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/terrains")
public class TerrainController {

    private final TerrainMapper terrainMapper;

    @Autowired
    public TerrainController(final TerrainMapper terrainMapper) {
        this.terrainMapper = terrainMapper;
    }

    @GetMapping("/getAllTerrains")
    public List<String> getTerrains() {
        return terrainMapper.findAll();
    }
}
