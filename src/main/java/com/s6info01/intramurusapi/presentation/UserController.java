package com.s6info01.intramurusapi.presentation;

import com.s6info01.intramurusapi.business.types.Membre;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @GetMapping
    @ResponseBody
    public Membre getCurrentUser(final HttpServletRequest httpRequest) {
        return (Membre) httpRequest.getAttribute("user");
    }
}
