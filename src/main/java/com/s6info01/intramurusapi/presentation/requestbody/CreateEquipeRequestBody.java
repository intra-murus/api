package com.s6info01.intramurusapi.presentation.requestbody;

public class CreateEquipeRequestBody {

    private String sport;
    private String nomLigue;
    private String nomEquipe;
    private String cipCapitaine;
    private String cipsJoueurs;

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getNomLigue() {
        return nomLigue;
    }

    public void setNomLigue(String nomLigue) {
        this.nomLigue = nomLigue;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public String getCipCapitaine() {
        return cipCapitaine;
    }

    public void setCipCapitaine(String cipCapitaine) {
        this.cipCapitaine = cipCapitaine;
    }

    public String getCipsJoueurs() {
        return cipsJoueurs;
    }

    public void setCipsJoueurs(String cipsJoueurs) {
        this.cipsJoueurs = cipsJoueurs;
    }

}
