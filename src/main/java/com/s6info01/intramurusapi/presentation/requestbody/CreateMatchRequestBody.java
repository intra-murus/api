package com.s6info01.intramurusapi.presentation.requestbody;

public class CreateMatchRequestBody {

    private Long idEquipeHome;
    private Long idEquipeAway;
    private Long dateDebut;
    private Long dateFin;
    private String terrain;
    private String sport;
    private String nomLigue;
    private String arbitre;

    public Long getIdEquipeHome() {
        return idEquipeHome;
    }

    public void setIdEquipeHome(Long idEquipeHome) {
        this.idEquipeHome = idEquipeHome;
    }

    public Long getIdEquipeAway() {
        return idEquipeAway;
    }

    public void setIdEquipeAway(Long idEquipeAway) {
        this.idEquipeAway = idEquipeAway;
    }

    public Long getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Long dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Long getDateFin() {
        return dateFin;
    }

    public void setDateFin(Long dateFin) {
        this.dateFin = dateFin;
    }

    public String getNomLigue() {
        return nomLigue;
    }

    public void setNomLigue(String nomLigue) {
        this.nomLigue = nomLigue;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getArbitre() {
        return arbitre;
    }

    public void setArbitre(String arbitre) {
        this.arbitre = arbitre;
    }
}
