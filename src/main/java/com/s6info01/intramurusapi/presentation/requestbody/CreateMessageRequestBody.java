package com.s6info01.intramurusapi.presentation.requestbody;

public class CreateMessageRequestBody {

    private String cipCapitaine;
    private String message;
    private long idEquipe;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public String getCipCapitaine() {
        return cipCapitaine;
    }

    public void setCipCapitaine(String cipCapitaine) {
        this.cipCapitaine = cipCapitaine;
    }
}
