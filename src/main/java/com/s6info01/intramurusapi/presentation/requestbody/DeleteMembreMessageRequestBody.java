package com.s6info01.intramurusapi.presentation.requestbody;

public class DeleteMembreMessageRequestBody {

    private String cip;
    private Long messageId;

    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }
}
