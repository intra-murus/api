package com.s6info01.intramurusapi.presentation.requestbody;

import com.s6info01.intramurusapi.business.types.EStatutApprobation;

public class UpdateStatutApprobationRequestBody {

    private Long idEquipe;
    private EStatutApprobation statutApprobation;

    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public EStatutApprobation getStatutApprobation() {
        return statutApprobation;
    }

    public void setStatutApprobation(EStatutApprobation statutApprobation) {
        this.statutApprobation = statutApprobation;
    }

}
