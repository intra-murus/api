package com.s6info01.intramurusapi.presentation.requestbody;

import com.s6info01.intramurusapi.business.types.EStatutEquipeMatch;

public class UpdateStatutEquipeMatchRequestBody {
    private Long idEquipe;
    private Long idMatch;
    private EStatutEquipeMatch statutEquipeMatch;


    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public Long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(Long idMatch) {
        this.idMatch = idMatch;
    }

    public EStatutEquipeMatch getStatutEquipeMatch() {
        return statutEquipeMatch;
    }

    public void setStatutEquipeMatch(EStatutEquipeMatch statutEquipeMatch) {
        this.statutEquipeMatch = statutEquipeMatch;
    }
}
