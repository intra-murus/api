package com.s6info01.intramurusapi.presentation.requestbody;

import com.s6info01.intramurusapi.business.types.EStatutMembreEquipe;

public class UpdateStatutMembreEquipeRequestBody {

    private String cip;
    private Long idEquipe;
    private EStatutMembreEquipe statutMembreEquipe;


    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public EStatutMembreEquipe getStatutMembreEquipe() {
        return statutMembreEquipe;
    }

    public void setStatutMembreEquipe(EStatutMembreEquipe statutMembreEquipe) {
        this.statutMembreEquipe = statutMembreEquipe;
    }

}
