package com.s6info01.intramurusapi.presentation.requestbody;

import com.s6info01.intramurusapi.business.types.EStatutMembreMatch;

public class UpdateStatutMembreMatchRequestBody {
    private String cip;
    private Long idMatch;
    private EStatutMembreMatch statutMembreMatch;


    public String getCip() {
        return cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public Long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(Long idMatch) {
        this.idMatch = idMatch;
    }

    public EStatutMembreMatch getStatutMembreMatch() {
        return statutMembreMatch;
    }

    public void setStatutMembreMatch(EStatutMembreMatch statutMembreMatch) {
        this.statutMembreMatch = statutMembreMatch;
    }
}

