package com.s6info01.intramurusapi.util;

import com.s6info01.intramurusapi.business.types.EPeriode;

import java.time.LocalDate;

public class SessionUtil {

    public static EPeriode getCurrentPeriode() {
        LocalDate currentDate = LocalDate.now();
        if(currentDate.getMonthValue() < 5) {
            return EPeriode.HIVER;
        } else if(currentDate.getMonthValue() < 9) {
            return EPeriode.ETE;
        } else {
            return EPeriode.AUTOMNE;
        }
    }
}
