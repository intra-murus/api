/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     11/6/2018 9:35:39 PM                         */
/*==============================================================*/


drop view MESSAGE_VIEW;

drop view MEMBRE_MATCH_VIEW;

drop view MATCH_VIEW;

drop view HORAIRE_ACCUEIL_VIEW;

drop view FILTRES_VIEW;

drop view EQUIPE_VIEW;

drop index COTE_PK;

drop table COTE cascade;

drop index STATUT_APPROB_EQUIPE_FK;

drop index LIGUE_EQUIPE_FK;

drop index EQUIPE_PK;

drop table EQUIPE cascade;

drop index EQUIPE_MATCH_FK4;

drop index EQUIPE_MATCH_FK3;

drop index EQUIPE_MATCH_FK2;

drop index EQUIPE_MATCH_FK;

drop index EQUIPE_MATCH_PK;

drop table EQUIPE_MATCH cascade;

drop index JOUR_SEMAINE_PK;

drop table JOUR_SEMAINE cascade;

drop index SESSION_LIGUE_FK;

drop index SPORT_LIGUE_FK;

drop index LIGUE_PK;

drop table LIGUE cascade;

drop index EST_DANS_FK;

drop index ARBITRE_FK;

drop index STATUT_MATCH_MATCH_FK;

drop index TERRAIN_MATCH_FK;

drop index MATCH_PK;

drop table MATCH cascade;

drop index MEMBRE_PK;

drop table MEMBRE cascade;

drop index MEMBRE_EQUIPE_FK3;

drop index MEMBRE_EQUIPE_FK2;

drop index MEMBRE_EQUIPE_FK;

drop index MEMBRE_EQUIPE_PK;

drop table MEMBRE_EQUIPE cascade;

drop index MEMBRE_MATCH_FK3;

drop index MEMBRE_MATCH_FK2;

drop index MEMBRE_MATCH_FK;

drop index MEMBRE_MATCH_PK;

drop table MEMBRE_MATCH cascade;

drop index MEMBRE_MESSAGE_FK2;

drop index MEMBRE_MESSAGE_FK;

drop index MEMBRE_MESSAGE_PK;

drop table MEMBRE_MESSAGE cascade;

drop index A_UN_FK;

drop index MESSAGES_PK;

drop table MESSAGES cascade;

drop index PERIODE_PK;

drop table PERIODE cascade;

drop index PERIODE_SPORT_FK2;

drop index PERIODE_SPORT_FK;

drop index PERIODE_SPORT_PK;

drop table PERIODE_SPORT cascade;

drop index PERIODE_SESSION_FK;

drop index SESSION_PK;

drop table SESSION cascade;

drop index SPORT_PK;

drop table SPORT cascade;

drop index SPORT_TERRAIN_DISPONIBILITE_FK3;

drop index SPORT_TERRAIN_DISPONIBILITE_FK2;

drop index SPORT_TERRAIN_DISPONIBILITE_FK;

drop index SPORT_TERRAIN_DISPONIBILITE_PK;

drop table SPORT_TERRAIN_DISPONIBILITE cascade;

drop index STATUT_APPROBATION_PK;

drop table STATUT_APPROBATION cascade;

drop index STATUT_EQUIPE_MATCH_PK;

drop table STATUT_EQUIPE_MATCH cascade;

drop index STATUT_MATCH_PK;

drop table STATUT_MATCH cascade;

drop index STATUT_MEMBRE_EQUIPE_PK;

drop table STATUT_MEMBRE_EQUIPE cascade;

drop index STATUT_MEMBRE_MATCH_PK;

drop table STATUT_MEMBRE_MATCH cascade;

drop index TERRAIN_PK;

drop table TERRAIN cascade;

drop index TYPE_DE_MEMBRE_PK;

drop table TYPE_DE_MEMBRE cascade;

drop index TYPE_MEMBRE_MEMBRE_FK2;

drop index TYPE_MEMBRE_MEMBRE_FK;

drop index TYPE_MEMBRE_MEMBRE_PK;

drop table TYPE_MEMBRE_MEMBRE cascade;

drop index TYPE_MESSAGE_PK;

drop table TYPE_MESSAGE cascade;

/*==============================================================*/
/* Table: COTE                                                  */
/*==============================================================*/
create table COTE (
   COTE                 VARCHAR(25)          not null,
   COULEUR_DOSSARD      VARCHAR(10)          not null,
   constraint PK_COTE primary key (COTE)
);

/*==============================================================*/
/* Index: COTE_PK                                               */
/*==============================================================*/
create unique index COTE_PK on COTE (
   COTE
);

/*==============================================================*/
/* Table: EQUIPE                                                */
/*==============================================================*/
create table EQUIPE (
   ID_EQUIPE            SERIAL               not null,
   STATUT_APPROBATION   VARCHAR(25)          not null,
   SPORT                VARCHAR(25)          not null,
   PERIODE              VARCHAR(10)          not null,
   ANNEE                INT2                 not null,
   NOM_LIGUE            VARCHAR(20)          not null,
   NOM_EQUIPE           VARCHAR(50)          not null,
   CIP_CAPITAINE        CHAR(8)              not null,
   constraint PK_EQUIPE primary key (ID_EQUIPE)
);

/*==============================================================*/
/* Index: EQUIPE_PK                                             */
/*==============================================================*/
create unique index EQUIPE_PK on EQUIPE (
   ID_EQUIPE
);

/*==============================================================*/
/* Index: LIGUE_EQUIPE_FK                                       */
/*==============================================================*/
create  index LIGUE_EQUIPE_FK on EQUIPE (
   SPORT,
   PERIODE,
   ANNEE,
   NOM_LIGUE
);

/*==============================================================*/
/* Index: STATUT_APPROB_EQUIPE_FK                               */
/*==============================================================*/
create  index STATUT_APPROB_EQUIPE_FK on EQUIPE (
   STATUT_APPROBATION
);

/*==============================================================*/
/* Table: EQUIPE_MATCH                                          */
/*==============================================================*/
create table EQUIPE_MATCH (
   ID_EQUIPE            INT4                 not null,
   ID_MATCH             INT4                 not null,
   STATUT_EQUIPE_MATCH  VARCHAR(25)          not null,
   COTE                 VARCHAR(25)          not null,
   constraint PK_EQUIPE_MATCH primary key (ID_EQUIPE, ID_MATCH, STATUT_EQUIPE_MATCH, COTE)
);

/*==============================================================*/
/* Index: EQUIPE_MATCH_PK                                       */
/*==============================================================*/
create unique index EQUIPE_MATCH_PK on EQUIPE_MATCH (
   ID_EQUIPE,
   ID_MATCH,
   STATUT_EQUIPE_MATCH,
   COTE
);

/*==============================================================*/
/* Index: EQUIPE_MATCH_FK                                       */
/*==============================================================*/
create  index EQUIPE_MATCH_FK on EQUIPE_MATCH (
   ID_EQUIPE
);

/*==============================================================*/
/* Index: EQUIPE_MATCH_FK2                                      */
/*==============================================================*/
create  index EQUIPE_MATCH_FK2 on EQUIPE_MATCH (
   ID_MATCH
);

/*==============================================================*/
/* Index: EQUIPE_MATCH_FK3                                      */
/*==============================================================*/
create  index EQUIPE_MATCH_FK3 on EQUIPE_MATCH (
   STATUT_EQUIPE_MATCH
);

/*==============================================================*/
/* Index: EQUIPE_MATCH_FK4                                      */
/*==============================================================*/
create  index EQUIPE_MATCH_FK4 on EQUIPE_MATCH (
   COTE
);

/*==============================================================*/
/* Table: JOUR_SEMAINE                                          */
/*==============================================================*/
create table JOUR_SEMAINE (
   JOUR                 VARCHAR(10)          not null,
   constraint PK_JOUR_SEMAINE primary key (JOUR)
);

/*==============================================================*/
/* Index: JOUR_SEMAINE_PK                                       */
/*==============================================================*/
create unique index JOUR_SEMAINE_PK on JOUR_SEMAINE (
   JOUR
);

/*==============================================================*/
/* Table: LIGUE                                                 */
/*==============================================================*/
create table LIGUE (
   SPORT                VARCHAR(25)          not null,
   PERIODE              VARCHAR(10)          not null,
   ANNEE                INT2                 not null,
   NOM_LIGUE            VARCHAR(20)          not null,
   NBR_MIN_EQUIPE       INT2                 not null,
   NBR_MAX_EQUIPE       INT2                 not null,
   constraint PK_LIGUE primary key (SPORT, PERIODE, ANNEE, NOM_LIGUE)
);

/*==============================================================*/
/* Index: LIGUE_PK                                              */
/*==============================================================*/
create unique index LIGUE_PK on LIGUE (
   SPORT,
   PERIODE,
   ANNEE,
   NOM_LIGUE
);

/*==============================================================*/
/* Index: SPORT_LIGUE_FK                                        */
/*==============================================================*/
create  index SPORT_LIGUE_FK on LIGUE (
   SPORT
);

/*==============================================================*/
/* Index: SESSION_LIGUE_FK                                      */
/*==============================================================*/
create  index SESSION_LIGUE_FK on LIGUE (
   PERIODE,
   ANNEE
);

/*==============================================================*/
/* Table: MATCH                                                 */
/*==============================================================*/
create table MATCH (
   ID_MATCH             SERIAL               not null,
   TERRAIN              VARCHAR(25)          not null,
   STATUT_MATCH         VARCHAR(25)          not null,
   SPORT                VARCHAR(25)          not null,
   PERIODE              VARCHAR(10)          not null,
   ANNEE                INT2                 not null,
   NOM_LIGUE            VARCHAR(20)          not null,
   CIP                  CHAR(8)              null,
   DATE_DEBUT           TIMESTAMP            not null,
   DATE_FIN             TIMESTAMP            not null,
   constraint PK_MATCH primary key (ID_MATCH)
);

/*==============================================================*/
/* Index: MATCH_PK                                              */
/*==============================================================*/
create unique index MATCH_PK on MATCH (
   ID_MATCH
);

/*==============================================================*/
/* Index: TERRAIN_MATCH_FK                                      */
/*==============================================================*/
create  index TERRAIN_MATCH_FK on MATCH (
   TERRAIN
);

/*==============================================================*/
/* Index: STATUT_MATCH_MATCH_FK                                 */
/*==============================================================*/
create  index STATUT_MATCH_MATCH_FK on MATCH (
   STATUT_MATCH
);

/*==============================================================*/
/* Index: ARBITRE_FK                                            */
/*==============================================================*/
create  index ARBITRE_FK on MATCH (
   CIP
);

/*==============================================================*/
/* Index: EST_DANS_FK                                           */
/*==============================================================*/
create  index EST_DANS_FK on MATCH (
   SPORT,
   PERIODE,
   ANNEE,
   NOM_LIGUE
);

/*==============================================================*/
/* Table: MEMBRE                                                */
/*==============================================================*/
create table MEMBRE (
   CIP                  CHAR(8)              not null,
   NOM                  VARCHAR(25)          not null,
   PRENOM               VARCHAR(25)          not null,
   EMAIL                VARCHAR(50)          not null,
   constraint PK_MEMBRE primary key (CIP)
);

/*==============================================================*/
/* Index: MEMBRE_PK                                             */
/*==============================================================*/
create unique index MEMBRE_PK on MEMBRE (
   CIP
);

/*==============================================================*/
/* Table: MEMBRE_EQUIPE                                         */
/*==============================================================*/
create table MEMBRE_EQUIPE (
   CIP                  CHAR(8)              not null,
   ID_EQUIPE            INT4                 not null,
   STATUT_MEMBRE_EQUIPE VARCHAR(25)          not null,
   constraint PK_MEMBRE_EQUIPE primary key (CIP, ID_EQUIPE, STATUT_MEMBRE_EQUIPE)
);

/*==============================================================*/
/* Index: MEMBRE_EQUIPE_PK                                      */
/*==============================================================*/
create unique index MEMBRE_EQUIPE_PK on MEMBRE_EQUIPE (
   CIP,
   ID_EQUIPE,
   STATUT_MEMBRE_EQUIPE
);

/*==============================================================*/
/* Index: MEMBRE_EQUIPE_FK                                      */
/*==============================================================*/
create  index MEMBRE_EQUIPE_FK on MEMBRE_EQUIPE (
   CIP
);

/*==============================================================*/
/* Index: MEMBRE_EQUIPE_FK2                                     */
/*==============================================================*/
create  index MEMBRE_EQUIPE_FK2 on MEMBRE_EQUIPE (
   ID_EQUIPE
);

/*==============================================================*/
/* Index: MEMBRE_EQUIPE_FK3                                     */
/*==============================================================*/
create  index MEMBRE_EQUIPE_FK3 on MEMBRE_EQUIPE (
   STATUT_MEMBRE_EQUIPE
);

/*==============================================================*/
/* Table: MEMBRE_MATCH                                          */
/*==============================================================*/
create table MEMBRE_MATCH (
   CIP                  CHAR(8)              not null,
   ID_MATCH             INT4                 not null,
   STATUT_MEMBRE_MATCH  VARCHAR(25)          not null,
   constraint PK_MEMBRE_MATCH primary key (CIP, ID_MATCH, STATUT_MEMBRE_MATCH)
);

/*==============================================================*/
/* Index: MEMBRE_MATCH_PK                                       */
/*==============================================================*/
create unique index MEMBRE_MATCH_PK on MEMBRE_MATCH (
   CIP,
   ID_MATCH,
   STATUT_MEMBRE_MATCH
);

/*==============================================================*/
/* Index: MEMBRE_MATCH_FK                                       */
/*==============================================================*/
create  index MEMBRE_MATCH_FK on MEMBRE_MATCH (
   CIP
);

/*==============================================================*/
/* Index: MEMBRE_MATCH_FK2                                      */
/*==============================================================*/
create  index MEMBRE_MATCH_FK2 on MEMBRE_MATCH (
   ID_MATCH
);

/*==============================================================*/
/* Index: MEMBRE_MATCH_FK3                                      */
/*==============================================================*/
create  index MEMBRE_MATCH_FK3 on MEMBRE_MATCH (
   STATUT_MEMBRE_MATCH
);

/*==============================================================*/
/* Table: MEMBRE_MESSAGE                                        */
/*==============================================================*/
create table MEMBRE_MESSAGE (
   ID_MESSAGE           INT4                 not null,
   CIP                  CHAR(8)              not null,
   constraint PK_MEMBRE_MESSAGE primary key (ID_MESSAGE, CIP)
);

/*==============================================================*/
/* Index: MEMBRE_MESSAGE_PK                                     */
/*==============================================================*/
create unique index MEMBRE_MESSAGE_PK on MEMBRE_MESSAGE (
   ID_MESSAGE,
   CIP
);

/*==============================================================*/
/* Index: MEMBRE_MESSAGE_FK                                     */
/*==============================================================*/
create  index MEMBRE_MESSAGE_FK on MEMBRE_MESSAGE (
   ID_MESSAGE
);

/*==============================================================*/
/* Index: MEMBRE_MESSAGE_FK2                                    */
/*==============================================================*/
create  index MEMBRE_MESSAGE_FK2 on MEMBRE_MESSAGE (
   CIP
);

/*==============================================================*/
/* Table: MESSAGES                                              */
/*==============================================================*/
create table MESSAGES (
   ID_MESSAGE           SERIAL               not null,
   TYPE_MESSAGE         VARCHAR(25)          not null,
   TEXT                 TEXT                 not null,
   LIEN                 INT4                 null,
   constraint PK_MESSAGES primary key (ID_MESSAGE)
);

/*==============================================================*/
/* Index: MESSAGES_PK                                           */
/*==============================================================*/
create unique index MESSAGES_PK on MESSAGES (
   ID_MESSAGE
);

/*==============================================================*/
/* Index: A_UN_FK                                               */
/*==============================================================*/
create  index A_UN_FK on MESSAGES (
   TYPE_MESSAGE
);

/*==============================================================*/
/* Table: PERIODE                                               */
/*==============================================================*/
create table PERIODE (
   PERIODE              VARCHAR(10)          not null,
   constraint PK_PERIODE primary key (PERIODE)
);

/*==============================================================*/
/* Index: PERIODE_PK                                            */
/*==============================================================*/
create unique index PERIODE_PK on PERIODE (
   PERIODE
);

/*==============================================================*/
/* Table: PERIODE_SPORT                                         */
/*==============================================================*/
create table PERIODE_SPORT (
   SPORT                VARCHAR(25)          not null,
   PERIODE              VARCHAR(10)          not null,
   constraint PK_PERIODE_SPORT primary key (SPORT, PERIODE)
);

/*==============================================================*/
/* Index: PERIODE_SPORT_PK                                      */
/*==============================================================*/
create unique index PERIODE_SPORT_PK on PERIODE_SPORT (
   SPORT,
   PERIODE
);

/*==============================================================*/
/* Index: PERIODE_SPORT_FK                                      */
/*==============================================================*/
create  index PERIODE_SPORT_FK on PERIODE_SPORT (
   SPORT
);

/*==============================================================*/
/* Index: PERIODE_SPORT_FK2                                     */
/*==============================================================*/
create  index PERIODE_SPORT_FK2 on PERIODE_SPORT (
   PERIODE
);

/*==============================================================*/
/* Table: SESSION                                               */
/*==============================================================*/
create table SESSION (
   PERIODE              VARCHAR(10)          not null,
   ANNEE                INT2                 not null,
   DATE_LIMITE_INSCRIPTION DATE                 not null,
   constraint PK_SESSION primary key (PERIODE, ANNEE)
);

/*==============================================================*/
/* Index: SESSION_PK                                            */
/*==============================================================*/
create unique index SESSION_PK on SESSION (
   PERIODE,
   ANNEE
);

/*==============================================================*/
/* Index: PERIODE_SESSION_FK                                    */
/*==============================================================*/
create  index PERIODE_SESSION_FK on SESSION (
   PERIODE
);

/*==============================================================*/
/* Table: SPORT                                                 */
/*==============================================================*/
create table SPORT (
   SPORT                VARCHAR(25)          not null,
   NBR_MIN_JOUEURS      INT2                 not null,
   A_DOSSARD            BOOL                 not null,
   A_ARBITRE            BOOL                 null,
   constraint PK_SPORT primary key (SPORT)
);

/*==============================================================*/
/* Index: SPORT_PK                                              */
/*==============================================================*/
create unique index SPORT_PK on SPORT (
   SPORT
);

/*==============================================================*/
/* Table: SPORT_TERRAIN_DISPONIBILITE                           */
/*==============================================================*/
create table SPORT_TERRAIN_DISPONIBILITE (
   TERRAIN              VARCHAR(25)          not null,
   SPORT                VARCHAR(25)          not null,
   JOUR                 VARCHAR(10)          not null,
   HEURE_DEBUT          TIME                 null,
   HEURE_FIN            TIME                 null,
   constraint PK_SPORT_TERRAIN_DISPONIBILITE primary key (TERRAIN, SPORT, JOUR)
);

/*==============================================================*/
/* Index: SPORT_TERRAIN_DISPONIBILITE_PK                        */
/*==============================================================*/
create unique index SPORT_TERRAIN_DISPONIBILITE_PK on SPORT_TERRAIN_DISPONIBILITE (
   TERRAIN,
   SPORT,
   JOUR
);

/*==============================================================*/
/* Index: SPORT_TERRAIN_DISPONIBILITE_FK                        */
/*==============================================================*/
create  index SPORT_TERRAIN_DISPONIBILITE_FK on SPORT_TERRAIN_DISPONIBILITE (
   TERRAIN
);

/*==============================================================*/
/* Index: SPORT_TERRAIN_DISPONIBILITE_FK2                       */
/*==============================================================*/
create  index SPORT_TERRAIN_DISPONIBILITE_FK2 on SPORT_TERRAIN_DISPONIBILITE (
   SPORT
);

/*==============================================================*/
/* Index: SPORT_TERRAIN_DISPONIBILITE_FK3                       */
/*==============================================================*/
create  index SPORT_TERRAIN_DISPONIBILITE_FK3 on SPORT_TERRAIN_DISPONIBILITE (
   JOUR
);

/*==============================================================*/
/* Table: STATUT_APPROBATION                                    */
/*==============================================================*/
create table STATUT_APPROBATION (
   STATUT_APPROBATION   VARCHAR(25)          not null,
   constraint PK_STATUT_APPROBATION primary key (STATUT_APPROBATION)
);

/*==============================================================*/
/* Index: STATUT_APPROBATION_PK                                 */
/*==============================================================*/
create unique index STATUT_APPROBATION_PK on STATUT_APPROBATION (
   STATUT_APPROBATION
);

/*==============================================================*/
/* Table: STATUT_EQUIPE_MATCH                                   */
/*==============================================================*/
create table STATUT_EQUIPE_MATCH (
   STATUT_EQUIPE_MATCH  VARCHAR(25)          not null,
   constraint PK_STATUT_EQUIPE_MATCH primary key (STATUT_EQUIPE_MATCH)
);

/*==============================================================*/
/* Index: STATUT_EQUIPE_MATCH_PK                                */
/*==============================================================*/
create unique index STATUT_EQUIPE_MATCH_PK on STATUT_EQUIPE_MATCH (
   STATUT_EQUIPE_MATCH
);

/*==============================================================*/
/* Table: STATUT_MATCH                                          */
/*==============================================================*/
create table STATUT_MATCH (
   STATUT_MATCH         VARCHAR(25)          not null,
   constraint PK_STATUT_MATCH primary key (STATUT_MATCH)
);

/*==============================================================*/
/* Index: STATUT_MATCH_PK                                       */
/*==============================================================*/
create unique index STATUT_MATCH_PK on STATUT_MATCH (
   STATUT_MATCH
);

/*==============================================================*/
/* Table: STATUT_MEMBRE_EQUIPE                                  */
/*==============================================================*/
create table STATUT_MEMBRE_EQUIPE (
   STATUT_MEMBRE_EQUIPE VARCHAR(25)          not null,
   constraint PK_STATUT_MEMBRE_EQUIPE primary key (STATUT_MEMBRE_EQUIPE)
);

/*==============================================================*/
/* Index: STATUT_MEMBRE_EQUIPE_PK                               */
/*==============================================================*/
create unique index STATUT_MEMBRE_EQUIPE_PK on STATUT_MEMBRE_EQUIPE (
   STATUT_MEMBRE_EQUIPE
);

/*==============================================================*/
/* Table: STATUT_MEMBRE_MATCH                                   */
/*==============================================================*/
create table STATUT_MEMBRE_MATCH (
   STATUT_MEMBRE_MATCH  VARCHAR(25)          not null,
   constraint PK_STATUT_MEMBRE_MATCH primary key (STATUT_MEMBRE_MATCH)
);

/*==============================================================*/
/* Index: STATUT_MEMBRE_MATCH_PK                                */
/*==============================================================*/
create unique index STATUT_MEMBRE_MATCH_PK on STATUT_MEMBRE_MATCH (
   STATUT_MEMBRE_MATCH
);

/*==============================================================*/
/* Table: TERRAIN                                               */
/*==============================================================*/
create table TERRAIN (
   TERRAIN              VARCHAR(25)          not null,
   constraint PK_TERRAIN primary key (TERRAIN)
);

/*==============================================================*/
/* Index: TERRAIN_PK                                            */
/*==============================================================*/
create unique index TERRAIN_PK on TERRAIN (
   TERRAIN
);

/*==============================================================*/
/* Table: TYPE_DE_MEMBRE                                        */
/*==============================================================*/
create table TYPE_DE_MEMBRE (
   TYPE_MEMBRE          VARCHAR(25)          not null,
   constraint PK_TYPE_DE_MEMBRE primary key (TYPE_MEMBRE)
);

/*==============================================================*/
/* Index: TYPE_DE_MEMBRE_PK                                     */
/*==============================================================*/
create unique index TYPE_DE_MEMBRE_PK on TYPE_DE_MEMBRE (
   TYPE_MEMBRE
);

/*==============================================================*/
/* Table: TYPE_MEMBRE_MEMBRE                                    */
/*==============================================================*/
create table TYPE_MEMBRE_MEMBRE (
   TYPE_MEMBRE          VARCHAR(25)          not null,
   CIP                  CHAR(8)              not null,
   constraint PK_TYPE_MEMBRE_MEMBRE primary key (TYPE_MEMBRE, CIP)
);

/*==============================================================*/
/* Index: TYPE_MEMBRE_MEMBRE_PK                                 */
/*==============================================================*/
create unique index TYPE_MEMBRE_MEMBRE_PK on TYPE_MEMBRE_MEMBRE (
   TYPE_MEMBRE,
   CIP
);

/*==============================================================*/
/* Index: TYPE_MEMBRE_MEMBRE_FK                                 */
/*==============================================================*/
create  index TYPE_MEMBRE_MEMBRE_FK on TYPE_MEMBRE_MEMBRE (
   TYPE_MEMBRE
);

/*==============================================================*/
/* Index: TYPE_MEMBRE_MEMBRE_FK2                                */
/*==============================================================*/
create  index TYPE_MEMBRE_MEMBRE_FK2 on TYPE_MEMBRE_MEMBRE (
   CIP
);

/*==============================================================*/
/* Table: TYPE_MESSAGE                                          */
/*==============================================================*/
create table TYPE_MESSAGE (
   TYPE_MESSAGE         VARCHAR(25)          not null,
   constraint PK_TYPE_MESSAGE primary key (TYPE_MESSAGE)
);

/*==============================================================*/
/* Index: TYPE_MESSAGE_PK                                       */
/*==============================================================*/
create unique index TYPE_MESSAGE_PK on TYPE_MESSAGE (
   TYPE_MESSAGE
);

/*==============================================================*/
/* View: EQUIPE_VIEW                                            */
/*==============================================================*/
create or replace view EQUIPE_VIEW as
   select mbr.*, mbreq.statut_membre_equipe, e.*
   from membre mbr
           inner join membre_equipe mbreq on mbr.cip = mbreq.cip
           inner join equipe e on mbreq.id_equipe = e.id_equipe
           inner join ligue l on e.sport = l.sport and e.periode = l.periode and e.annee = l.annee and e.nom_ligue = l.nom_ligue;

/*==============================================================*/
/* View: FILTRES_VIEW                                           */
/*==============================================================*/
create or replace view FILTRES_VIEW as
   select p.periode, se.annee, ps.sport, l.nom_ligue
   from periode p
           inner join session se on p.periode = se.periode
           inner join periode_sport ps on se.periode = ps.periode
           inner join ligue l on ps.sport = l.sport;

/*==============================================================*/
/* View: HORAIRE_ACCUEIL_VIEW                                   */
/*==============================================================*/
create or replace view HORAIRE_ACCUEIL_VIEW as
  select me.cip, mm.statut_membre_match, m.id_match, m.terrain, m.statut_match, m.date_debut, m.date_fin, e.sport, l.nom_ligue, e.nom_equipe
  from match m
         inner join equipe_match em on m.id_match = em.id_match
         inner join equipe e on em.id_equipe = e.id_equipe
         inner join ligue l on e.nom_ligue = l.nom_ligue and e.periode = l.periode and e.annee = l.annee
         inner join membre_equipe me on e.id_equipe = me.id_equipe
         inner join membre_match mm on me.cip = mm.cip and mm.id_match = m.id_match;

/*==============================================================*/
/* View: MATCH_VIEW                                             */
/*==============================================================*/
create or replace view MATCH_VIEW as
   SELECT m.id_match, m.statut_match, m.sport, m.nom_ligue, m.periode, m.annee,  m.date_debut, m.date_fin, m.terrain, m.cip as arbitre,
          e.id_equipe, e.nom_equipe, e.cip_capitaine,
          em.statut_equipe_match, em.cote
   FROM match m
           INNER JOIN equipe_match em ON em.id_match = m.id_match
           INNER JOIN equipe e ON e.id_equipe = em.id_equipe;

/*==============================================================*/
/* View: MEMBRE_MATCH_VIEW                                      */
/*==============================================================*/
create or replace view MEMBRE_MATCH_VIEW as
   SELECT mbr.cip, mbr.prenom, mbr.nom, mm.statut_membre_match, e.id_equipe, m.id_match
   FROM match m
           INNER JOIN equipe_match em ON em.id_match = m.id_match
           INNER JOIN equipe e ON e.id_equipe = em.id_equipe
           INNER JOIN membre_equipe me on e.id_equipe = me.id_equipe
           INNER JOIN membre_match mm on mm.id_match = m.id_match AND mm.cip =  me.cip
           INNER JOIN membre mbr on mbr.cip = me.cip;

/*==============================================================*/
/* View: MESSAGE_VIEW                                           */
/*==============================================================*/
create or replace view MESSAGE_VIEW as
   SELECT mbr_m.cip, m.*
   FROM messages m
           INNER JOIN membre_message mbr_m on m.id_message = mbr_m.id_message;

alter table EQUIPE
   add constraint FK_EQUIPE_LIGUE_EQU_LIGUE foreign key (SPORT, PERIODE, ANNEE, NOM_LIGUE)
references LIGUE (SPORT, PERIODE, ANNEE, NOM_LIGUE)
on delete restrict on update restrict;

alter table EQUIPE
   add constraint FK_EQUIPE_STATUT_AP_STATUT_A foreign key (STATUT_APPROBATION)
references STATUT_APPROBATION (STATUT_APPROBATION)
on delete restrict on update restrict;

alter table EQUIPE_MATCH
   add constraint FK_EQUIPE_M_EQUIPE_MA_COTE foreign key (COTE)
references COTE (COTE)
on delete restrict on update restrict;

alter table EQUIPE_MATCH
   add constraint FK_EQUIPE_M_EQUIPE_MA_EQUIPE foreign key (ID_EQUIPE)
references EQUIPE (ID_EQUIPE)
on delete restrict on update restrict;

alter table EQUIPE_MATCH
   add constraint FK_EQUIPE_M_EQUIPE_MA_MATCH foreign key (ID_MATCH)
references MATCH (ID_MATCH)
on delete restrict on update restrict;

alter table EQUIPE_MATCH
   add constraint FK_EQUIPE_M_EQUIPE_MA_STATUT_E foreign key (STATUT_EQUIPE_MATCH)
references STATUT_EQUIPE_MATCH (STATUT_EQUIPE_MATCH)
on delete restrict on update restrict;

alter table LIGUE
   add constraint FK_LIGUE_SESSION_L_SESSION foreign key (PERIODE, ANNEE)
references SESSION (PERIODE, ANNEE)
on delete restrict on update restrict;

alter table LIGUE
   add constraint FK_LIGUE_SPORT_LIG_SPORT foreign key (SPORT)
references SPORT (SPORT)
on delete restrict on update restrict;

alter table MATCH
   add constraint FK_MATCH_ARBITRE_MEMBRE foreign key (CIP)
references MEMBRE (CIP)
on delete restrict on update restrict;

alter table MATCH
   add constraint FK_MATCH_EST_DANS_LIGUE foreign key (SPORT, PERIODE, ANNEE, NOM_LIGUE)
references LIGUE (SPORT, PERIODE, ANNEE, NOM_LIGUE)
on delete restrict on update restrict;

alter table MATCH
   add constraint FK_MATCH_STATUT_MA_STATUT_M foreign key (STATUT_MATCH)
references STATUT_MATCH (STATUT_MATCH)
on delete restrict on update restrict;

alter table MATCH
   add constraint FK_MATCH_TERRAIN_M_TERRAIN foreign key (TERRAIN)
references TERRAIN (TERRAIN)
on delete restrict on update restrict;

alter table MEMBRE_EQUIPE
   add constraint FK_MEMBRE_E_MEMBRE_EQ_EQUIPE foreign key (ID_EQUIPE)
references EQUIPE (ID_EQUIPE)
on delete restrict on update restrict;

alter table MEMBRE_EQUIPE
   add constraint FK_MEMBRE_E_MEMBRE_EQ_MEMBRE foreign key (CIP)
references MEMBRE (CIP)
on delete restrict on update restrict;

alter table MEMBRE_EQUIPE
   add constraint FK_MEMBRE_E_MEMBRE_EQ_STATUT_M foreign key (STATUT_MEMBRE_EQUIPE)
references STATUT_MEMBRE_EQUIPE (STATUT_MEMBRE_EQUIPE)
on delete restrict on update restrict;

alter table MEMBRE_MATCH
   add constraint FK_MEMBRE_M_MEMBRE_MA_MATCH foreign key (ID_MATCH)
references MATCH (ID_MATCH)
on delete restrict on update restrict;

alter table MEMBRE_MATCH
   add constraint FK_MEMBRE_M_MEMBRE_MA_MEMBRE foreign key (CIP)
references MEMBRE (CIP)
on delete restrict on update restrict;

alter table MEMBRE_MATCH
   add constraint FK_MEMBRE_M_MEMBRE_MA_STATUT_M foreign key (STATUT_MEMBRE_MATCH)
references STATUT_MEMBRE_MATCH (STATUT_MEMBRE_MATCH)
on delete restrict on update restrict;

alter table MEMBRE_MESSAGE
   add constraint FK_MEMBRE_M_MEMBRE_ME_MEMBRE foreign key (CIP)
references MEMBRE (CIP)
on delete restrict on update restrict;

alter table MEMBRE_MESSAGE
   add constraint FK_MEMBRE_M_MEMBRE_ME_MESSAGES foreign key (ID_MESSAGE)
references MESSAGES (ID_MESSAGE)
on delete restrict on update restrict;

alter table MESSAGES
   add constraint FK_MESSAGES_A_UN_TYPE_MES foreign key (TYPE_MESSAGE)
references TYPE_MESSAGE (TYPE_MESSAGE)
on delete restrict on update restrict;

alter table PERIODE_SPORT
   add constraint FK_PERIODE__PERIODE_S_PERIODE foreign key (PERIODE)
references PERIODE (PERIODE)
on delete restrict on update restrict;

alter table PERIODE_SPORT
   add constraint FK_PERIODE__PERIODE_S_SPORT foreign key (SPORT)
references SPORT (SPORT)
on delete restrict on update restrict;

alter table SESSION
   add constraint FK_SESSION_PERIODE_S_PERIODE foreign key (PERIODE)
references PERIODE (PERIODE)
on delete restrict on update restrict;

alter table SPORT_TERRAIN_DISPONIBILITE
   add constraint FK_SPORT_TE_SPORT_TER_JOUR_SEM foreign key (JOUR)
references JOUR_SEMAINE (JOUR)
on delete restrict on update restrict;

alter table SPORT_TERRAIN_DISPONIBILITE
   add constraint FK_SPORT_TE_SPORT_TER_SPORT foreign key (SPORT)
references SPORT (SPORT)
on delete restrict on update restrict;

alter table SPORT_TERRAIN_DISPONIBILITE
   add constraint FK_SPORT_TE_SPORT_TER_TERRAIN foreign key (TERRAIN)
references TERRAIN (TERRAIN)
on delete restrict on update restrict;

alter table TYPE_MEMBRE_MEMBRE
   add constraint FK_TYPE_MEM_TYPE_MEMB_MEMBRE foreign key (CIP)
references MEMBRE (CIP)
on delete restrict on update restrict;

alter table TYPE_MEMBRE_MEMBRE
   add constraint FK_TYPE_MEM_TYPE_MEMB_TYPE_DE_ foreign key (TYPE_MEMBRE)
references TYPE_DE_MEMBRE (TYPE_MEMBRE)
on delete restrict on update restrict;


-- DATA

INSERT INTO jour_semaine(JOUR)
VALUES ('DIMANCHE'),
       ('LUNDI'),
       ('MARDI'),
       ('MERCREDI'),
       ('JEUDI'),
       ('VENDREDI'),
       ('SAMEDI');

INSERT INTO type_de_membre(TYPE_MEMBRE)
VALUES ('JOUEUR'),
       ('ADMINISTRATEUR');

INSERT INTO statut_approbation(STATUT_APPROBATION)
VALUES ('APPROUVE'),
       ('EN_ATTENTE'),
       ('PRET_A_APPROUVER'),
       ('REFUSE');

INSERT INTO statut_membre_equipe(STATUT_MEMBRE_EQUIPE)
VALUES ('ACCEPTE'),
       ('EN_ATTENTE'),
       ('REFUSE');

INSERT INTO statut_equipe_match(STATUT_EQUIPE_MATCH)
VALUES ('PRESENT'),
       ('EN_ATTENTE'),
       ('ABSENT');

INSERT INTO statut_match(STATUT_MATCH)
VALUES ('CONFIRME'),
       ('EN_ATTENTE'),
       ('ANNULE');

INSERT INTO statut_membre_match(STATUT_MEMBRE_MATCH)
VALUES ('PRESENT'),
       ('EN_ATTENTE'),
       ('ABSENT');

INSERT INTO type_message(TYPE_MESSAGE)
VALUES ('INVITATION'),
       ('CONFIRMATION'),
       ('EN_ATTENTE'),
       ('TEXTE');

INSERT INTO periode(PERIODE)
VALUES ('AUTOMNE'),
       ('HIVER'),
       ('ETE');

INSERT INTO terrain(TERRAIN)
VALUES ('ANNEXE'),
       ('CONCORDE');

INSERT INTO membre(CIP, NOM, PRENOM, EMAIL)
VALUES ('test1234', 'Test', 'Test', 'test@usherbrooke.ca'),
       ('doej4321', 'Doe', 'Jon', 'jon.doe@usherbrooke.ca'),
       ('masm8385', 'Masse', 'Manon', 'manon.masse@usherbrooke.ca'),
       ('nadg1929', 'Nadeau-Dubois', 'Gabriel', 'gabriel.nadeau-dubois@usherbrooke.ca'),
       ('chaj9515', 'Charest', 'Jean', 'jean.charest@usherbrooke.ca'),
       ('coup1852', 'Couillard', 'Philippe', 'philippe.couillard@usherbrooke.ca'),
       ('lisj2587', 'Lise', 'Jean-Francois', 'jean-francois.lise@usherbrooke.ca'),
       ('legf5917', 'Legault', 'Francois', 'francois.legault@usherbrooke.ca'),
       ('carm1234', 'Matos', 'Carlos', 'carlos.matos@usherbrooke.ca'),
       ('mmes1234', 'Mmer', 'Sca', 'sca.mmer@usherbrooke.ca'),
       ('riom1234', 'Rio', 'Ma', 'ma.rio@usherbrooke.ca'),
       ('twom1234', 'Two', 'Mew', 'mew.two@usherbrooke.ca'),
       ('chup1234', 'Chu', 'Pika', 'pika.chu@usherbrooke.ca'),
       ('falc1234', 'Falcon', 'Captain', 'captain.falcon@usherbrooke.ca'),
       ('mans1234', 'Man', 'Spider', 'spider.man@usherbrooke.ca'),
       ('manb1234', 'Man', 'Bat', 'bat.man@usherbrooke.ca'),
       ('mora8951', 'Morgan', 'Arthur', 'arthur.morgan@usherbrooke.ca'),
       ('adls9512', 'Adler', 'Sadie', 'sadie.adler@usherbrooke.ca'),
       ('vand1415', 'Van der Linde','Dutch', 'dutch.vanderlinde@usherbrooke.ca'),
       ('marj1435', 'Marston', 'John', 'john.marston@usherbrooke.ca'),
       ('smic9591', 'Smith', 'Charles', 'charles.smith@usherbrooke.ca'),
       ('roba1245', 'Roberts', 'Abigail', 'abigail.roberts@usherbrooke.ca'),
       ('belm1524', 'Bell', 'Micah', 'micah.bell@usherbrooke.ca'),
       ('escj9672', 'Escuela', 'Javier', 'javier.escuela@usherbrooke.ca'),
       ('mari0194', '', 'Mario', 'mario@usherbrooke.ca'),
       ('luig9512', '', 'Luigi', 'luigi@usherbrooke.ca'),
       ('kirb1415', '', 'Kirby', 'kirby@usherbrooke.ca'),
       ('samu1435', '', 'Samus', 'samus@usherbrooke.ca'),
       ('mart9591', '', 'Marth', 'marth@usherbrooke.ca'),
       ('robi1245', '', 'Robin', 'robin.roberts@usherbrooke.ca'),
       ('puff1524', '', 'Jigglypuff', 'jigglypuff@usherbrooke.ca'),
       ('bows9672', '', 'Bowser', 'bowser@usherbrooke.ca');

INSERT INTO type_membre_membre(TYPE_MEMBRE, CIP)
VALUES ('JOUEUR', 'test1234'),
       ('JOUEUR', 'doej4321'),
       ('JOUEUR', 'masm8385'),
       ('JOUEUR', 'nadg1929'),
       ('JOUEUR', 'chaj9515'),
       ('JOUEUR', 'coup1852'),
       ('JOUEUR', 'lisj2587'),
       ('JOUEUR', 'legf5917'),
       ('JOUEUR', 'carm1234'),
       ('JOUEUR', 'mmes1234'),
       ('JOUEUR', 'riom1234'),
       ('JOUEUR', 'twom1234'),
       ('JOUEUR', 'chup1234'),
       ('JOUEUR', 'falc1234'),
       ('JOUEUR', 'mans1234'),
       ('JOUEUR', 'manb1234'),
       ('JOUEUR', 'mora8951'),
       ('JOUEUR', 'adls9512'),
       ('JOUEUR', 'vand1415'),
       ('JOUEUR', 'marj1435'),
       ('JOUEUR', 'smic9591'),
       ('JOUEUR', 'roba1245'),
       ('JOUEUR', 'belm1524'),
       ('JOUEUR', 'escj9672'),
       ('JOUEUR', 'mari0194'),
       ('JOUEUR', 'luig9512'),
       ('JOUEUR', 'kirb1415'),
       ('JOUEUR', 'samu1435'),
       ('JOUEUR', 'mart9591'),
       ('JOUEUR', 'robi1245'),
       ('JOUEUR', 'puff1524'),
       ('JOUEUR', 'bows9672');


INSERT INTO sport(SPORT, NBR_MIN_JOUEURS, A_DOSSARD, A_ARBITRE)
VALUES ('SOCCER_INTERIEUR', '7', true, true),
       ('SOCCER_EXTERIEUR', '11', true, true),
       ('BASKETBALL', '7', true, true),
       ('FRISBEE_INTERIEUR', '7', false, false),
       ('FRISBEE_EXTERIEUR', '7', false, false),
       ('HOCKEY_COSOM', '5', true, true),
       ('HOCKEY_EXTERIEUR', '5', true, true),
       ('WATERPOLO', '7', false, true);

INSERT INTO periode_sport(SPORT, PERIODE)
VALUES ('SOCCER_INTERIEUR', 'AUTOMNE'),
       ('SOCCER_INTERIEUR', 'HIVER'),
       ('SOCCER_EXTERIEUR', 'ETE'),
       ('BASKETBALL', 'AUTOMNE'),
       ('BASKETBALL', 'HIVER'),
       ('FRISBEE_INTERIEUR', 'AUTOMNE'),
       ('FRISBEE_INTERIEUR', 'HIVER'),
       ('FRISBEE_EXTERIEUR', 'ETE'),
       ('HOCKEY_COSOM', 'AUTOMNE'),
       ('HOCKEY_COSOM', 'HIVER'),
       ('HOCKEY_EXTERIEUR', 'HIVER'),
       ('WATERPOLO', 'AUTOMNE'),
       ('WATERPOLO', 'HIVER'),
       ('WATERPOLO', 'ETE');

INSERT INTO session(PERIODE, ANNEE, DATE_LIMITE_INSCRIPTION)
VALUES ('AUTOMNE', 2018, '2018-09-05'),
       ('HIVER', 2018, '2018-01-15'),
       ('ETE', 2018, '2018-04-15');

INSERT INTO ligue(SPORT, PERIODE, ANNEE, NOM_LIGUE, NBR_MIN_EQUIPE, NBR_MAX_EQUIPE)
VALUES ('SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', '6', '20'),
       ('SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'A', '6', '20'),
       ('SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'B', '6', '20'),
       ('SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'C', '6', '20'),
       ('SOCCER_INTERIEUR', 'HIVER', 2018, 'AA', '6', '20'),
       ('SOCCER_INTERIEUR', 'HIVER', 2018, 'A', '6', '20'),
       ('SOCCER_INTERIEUR', 'HIVER', 2018, 'B', '6', '20'),
       ('SOCCER_INTERIEUR', 'HIVER', 2018, 'C', '6', '20'),
       ('SOCCER_EXTERIEUR', 'ETE', 2018, 'A', '6', '20'),
       ('SOCCER_EXTERIEUR', 'ETE', 2018, 'B', '6', '20'),
       ('SOCCER_EXTERIEUR', 'ETE', 2018, 'C', '6', '20'),
       ('HOCKEY_COSOM', 'AUTOMNE', 2018, 'A', '6', '20'),
       ('HOCKEY_COSOM', 'AUTOMNE', 2018, 'B', '6', '20'),
       ('HOCKEY_COSOM', 'ETE', 2018, 'A', '6', '20'),
       ('HOCKEY_COSOM', 'ETE', 2018, 'B', '6', '20'),
       ('BASKETBALL', 'AUTOMNE', 2018, 'A', '6', '20'),
       ('BASKETBALL', 'AUTOMNE', 2018, 'B', '6', '20');

INSERT INTO cote(COTE, COULEUR_DOSSARD)
VALUES ('HOME', 'VERT'),
       ('AWAY', 'JAUNE');

INSERT INTO equipe(STATUT_APPROBATION, SPORT, PERIODE, ANNEE, NOM_LIGUE, NOM_EQUIPE, CIP_CAPITAINE)
VALUES ('EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'SCRUBTUS', 'test1234'),
       ('EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'BITCONNECT', 'carm1234'),
       ('EN_ATTENTE', 'HOCKEY_COSOM', 'AUTOMNE', 2018, 'A', 'CANADIENS', 'legf5917'),
       ('EN_ATTENTE', 'HOCKEY_COSOM', 'AUTOMNE', 2018, 'A', 'LEAVES', 'mmes1234'),
       ('EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'DUTCH CREW', 'vand1415'),
       ('EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'SMASH BROS', 'mari0194');

INSERT INTO membre_equipe(ID_EQUIPE, CIP, STATUT_MEMBRE_EQUIPE)
VALUES (1, 'test1234', 'EN_ATTENTE'),
       (1, 'doej4321', 'EN_ATTENTE'),
       (1, 'masm8385', 'EN_ATTENTE'),
       (1, 'nadg1929', 'EN_ATTENTE'),
       (1, 'chaj9515', 'EN_ATTENTE'),
       (1, 'coup1852', 'EN_ATTENTE'),
       (1, 'lisj2587', 'EN_ATTENTE'),
       (1, 'legf5917', 'EN_ATTENTE'),
       (2, 'carm1234', 'EN_ATTENTE'),
       (2, 'mmes1234', 'EN_ATTENTE'),
       (2, 'riom1234', 'EN_ATTENTE'),
       (2, 'twom1234', 'EN_ATTENTE'),
       (2, 'chup1234', 'EN_ATTENTE'),
       (2, 'falc1234', 'EN_ATTENTE'),
       (2, 'mans1234', 'EN_ATTENTE'),
       (2, 'manb1234', 'EN_ATTENTE'),
       (3, 'test1234', 'EN_ATTENTE'),
       (3, 'doej4321', 'EN_ATTENTE'),
       (3, 'masm8385', 'EN_ATTENTE'),
       (3, 'nadg1929', 'EN_ATTENTE'),
       (3, 'chaj9515', 'EN_ATTENTE'),
       (3, 'coup1852', 'EN_ATTENTE'),
       (3, 'lisj2587', 'EN_ATTENTE'),
       (3, 'legf5917', 'EN_ATTENTE'),
       (4, 'carm1234', 'ACCEPTE'),
       (4, 'mmes1234', 'EN_ATTENTE'),
       (4, 'riom1234', 'ACCEPTE'),
       (4, 'twom1234', 'ACCEPTE'),
       (4, 'chup1234', 'ACCEPTE'),
       (4, 'falc1234', 'REFUSE'),
       (4, 'mans1234', 'ACCEPTE'),
       (4, 'manb1234', 'REFUSE'),
       (5, 'mora8951', 'EN_ATTENTE'),
       (5, 'adls9512', 'EN_ATTENTE'),
       (5, 'vand1415', 'EN_ATTENTE'),
       (5, 'marj1435', 'EN_ATTENTE'),
       (5, 'smic9591', 'EN_ATTENTE'),
       (5, 'roba1245', 'EN_ATTENTE'),
       (5, 'belm1524', 'EN_ATTENTE'),
       (5, 'escj9672', 'EN_ATTENTE'),
       (6, 'mari0194', 'EN_ATTENTE'),
       (6, 'luig9512', 'EN_ATTENTE'),
       (6, 'kirb1415', 'EN_ATTENTE'),
       (6, 'samu1435', 'EN_ATTENTE'),
       (6, 'mart9591', 'EN_ATTENTE'),
       (6, 'robi1245', 'EN_ATTENTE'),
       (6, 'puff1524', 'EN_ATTENTE'),
       (6, 'bows9672', 'EN_ATTENTE');

INSERT INTO match(TERRAIN, STATUT_MATCH, SPORT, PERIODE, ANNEE, NOM_LIGUE, CIP, DATE_DEBUT, DATE_FIN)
VALUES ('ANNEXE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-20 18:00:00', '2018-11-20 19:00:00'),
       ('ANNEXE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-20 20:00:00', '2018-11-20 21:00:00'),
       ('ANNEXE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-22 21:00:00', '2018-11-22 22:00:00'),
       ('ANNEXE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-22 22:00:00', '2018-11-22 23:00:00'),
       ('CONCORDE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-23 17:00:00', '2018-11-23 18:00:00'),
       ('CONCORDE', 'EN_ATTENTE', 'SOCCER_INTERIEUR', 'AUTOMNE', 2018, 'AA', 'test1234', '2018-11-23 18:00:00', '2018-11-23 19:00:00');

INSERT INTO equipe_match(ID_EQUIPE, ID_MATCH, STATUT_EQUIPE_MATCH, COTE)
VALUES(1, 1, 'PRESENT', 'HOME'),
      (2, 1, 'PRESENT', 'AWAY'),
      (5, 2, 'PRESENT', 'AWAY'),
      (6, 2, 'PRESENT', 'HOME'),
      (2, 3, 'PRESENT', 'HOME'),
      (5, 3, 'PRESENT', 'AWAY'),
      (1, 4, 'PRESENT', 'HOME'),
      (6, 4, 'EN_ATTENTE', 'AWAY'),
      (2, 5, 'EN_ATTENTE', 'HOME'),
      (1, 5, 'PRESENT', 'AWAY'),
      (6, 6, 'EN_ATTENTE', 'HOME'),
      (5, 6, 'PRESENT', 'AWAY');

INSERT INTO membre_match(cip, id_match, statut_membre_match)
VALUES ('test1234', 1, 'EN_ATTENTE'),
       ('doej4321', 1, 'EN_ATTENTE'),
       ('masm8385', 1, 'EN_ATTENTE'),
       ('nadg1929', 1, 'EN_ATTENTE'),
       ('chaj9515', 1, 'EN_ATTENTE'),
       ('coup1852', 1, 'EN_ATTENTE'),
       ('lisj2587', 1, 'EN_ATTENTE'),
       ('legf5917', 1, 'EN_ATTENTE'),
       ('test1234', 4, 'EN_ATTENTE'),
       ('doej4321', 4, 'EN_ATTENTE'),
       ('masm8385', 4, 'EN_ATTENTE'),
       ('nadg1929', 4, 'EN_ATTENTE'),
       ('chaj9515', 4, 'EN_ATTENTE'),
       ('coup1852', 4, 'EN_ATTENTE'),
       ('lisj2587', 4, 'EN_ATTENTE'),
       ('legf5917', 4, 'EN_ATTENTE'),
       ('test1234', 5, 'EN_ATTENTE'),
       ('doej4321', 5, 'EN_ATTENTE'),
       ('masm8385', 5, 'EN_ATTENTE'),
       ('nadg1929', 5, 'EN_ATTENTE'),
       ('chaj9515', 5, 'EN_ATTENTE'),
       ('coup1852', 5, 'EN_ATTENTE'),
       ('lisj2587', 5, 'EN_ATTENTE'),
       ('legf5917', 5, 'EN_ATTENTE'),

       ('carm1234', 1, 'EN_ATTENTE'),
       ('mmes1234', 1, 'EN_ATTENTE'),
       ('riom1234', 1, 'EN_ATTENTE'),
       ('twom1234', 1, 'EN_ATTENTE'),
       ('chup1234', 1, 'EN_ATTENTE'),
       ('falc1234', 1, 'EN_ATTENTE'),
       ('mans1234', 1, 'EN_ATTENTE'),
       ('manb1234', 1, 'EN_ATTENTE'),
       ('carm1234', 3, 'EN_ATTENTE'),
       ('mmes1234', 3, 'EN_ATTENTE'),
       ('riom1234', 3, 'EN_ATTENTE'),
       ('twom1234', 3, 'EN_ATTENTE'),
       ('chup1234', 3, 'EN_ATTENTE'),
       ('falc1234', 3, 'EN_ATTENTE'),
       ('mans1234', 3, 'EN_ATTENTE'),
       ('manb1234', 3, 'EN_ATTENTE'),
       ('carm1234', 5, 'EN_ATTENTE'),
       ('mmes1234', 5, 'EN_ATTENTE'),
       ('riom1234', 5, 'EN_ATTENTE'),
       ('twom1234', 5, 'EN_ATTENTE'),
       ('chup1234', 5, 'EN_ATTENTE'),
       ('falc1234', 5, 'EN_ATTENTE'),
       ('mans1234', 5, 'EN_ATTENTE'),
       ('manb1234', 5, 'EN_ATTENTE'),

       ('mora8951', 2, 'EN_ATTENTE'),
       ('adls9512', 2, 'EN_ATTENTE'),
       ('vand1415', 2, 'EN_ATTENTE'),
       ('marj1435', 2, 'EN_ATTENTE'),
       ('smic9591', 2, 'EN_ATTENTE'),
       ('roba1245', 2, 'EN_ATTENTE'),
       ('belm1524', 2, 'EN_ATTENTE'),
       ('escj9672', 2, 'EN_ATTENTE'),
       ('mora8951', 3, 'EN_ATTENTE'),
       ('adls9512', 3, 'EN_ATTENTE'),
       ('vand1415', 3, 'EN_ATTENTE'),
       ('marj1435', 3, 'EN_ATTENTE'),
       ('smic9591', 3, 'EN_ATTENTE'),
       ('roba1245', 3, 'EN_ATTENTE'),
       ('belm1524', 3, 'EN_ATTENTE'),
       ('escj9672', 3, 'EN_ATTENTE'),
       ('mora8951', 6, 'EN_ATTENTE'),
       ('adls9512', 6, 'EN_ATTENTE'),
       ('vand1415', 6, 'EN_ATTENTE'),
       ('marj1435', 6, 'EN_ATTENTE'),
       ('smic9591', 6, 'EN_ATTENTE'),
       ('roba1245', 6, 'EN_ATTENTE'),
       ('belm1524', 6, 'EN_ATTENTE'),
       ('escj9672', 6, 'EN_ATTENTE'),


       ('mari0194', 2, 'EN_ATTENTE'),
       ('luig9512', 2, 'EN_ATTENTE'),
       ('kirb1415', 2, 'EN_ATTENTE'),
       ('samu1435', 2, 'EN_ATTENTE'),
       ('mart9591', 2, 'EN_ATTENTE'),
       ('robi1245', 2, 'EN_ATTENTE'),
       ('puff1524', 2, 'EN_ATTENTE'),
       ('bows9672', 2, 'EN_ATTENTE'),
       ('mari0194', 4, 'EN_ATTENTE'),
       ('luig9512', 4, 'EN_ATTENTE'),
       ('kirb1415', 4, 'EN_ATTENTE'),
       ('samu1435', 4, 'EN_ATTENTE'),
       ('mart9591', 4, 'EN_ATTENTE'),
       ('robi1245', 4, 'EN_ATTENTE'),
       ('puff1524', 4, 'EN_ATTENTE'),
       ('bows9672', 4, 'EN_ATTENTE'),
       ('mari0194', 6, 'EN_ATTENTE'),
       ('luig9512', 6, 'EN_ATTENTE'),
       ('kirb1415', 6, 'EN_ATTENTE'),
       ('samu1435', 6, 'EN_ATTENTE'),
       ('mart9591', 6, 'EN_ATTENTE'),
       ('robi1245', 6, 'EN_ATTENTE'),
       ('puff1524', 6, 'EN_ATTENTE'),
       ('bows9672', 6, 'EN_ATTENTE');

UPDATE membre_equipe SET statut_membre_equipe = 'ACCEPTE' WHERE id_equipe = 1;
UPDATE membre_equipe SET statut_membre_equipe = 'ACCEPTE' WHERE id_equipe = 2;
UPDATE membre_equipe SET statut_membre_equipe = 'ACCEPTE' WHERE id_equipe = 3;
UPDATE membre_equipe SET statut_membre_equipe = 'ACCEPTE' WHERE id_equipe = 5;
UPDATE membre_equipe SET statut_membre_equipe = 'ACCEPTE' WHERE id_equipe = 6;

UPDATE equipe SET statut_approbation = 'PRET_A_APPROUVER' WHERE id_equipe = 1;
UPDATE equipe SET statut_approbation = 'PRET_A_APPROUVER' WHERE id_equipe = 2;
UPDATE equipe SET statut_approbation = 'PRET_A_APPROUVER' WHERE id_equipe = 3;
UPDATE equipe SET statut_approbation = 'PRET_A_APPROUVER' WHERE id_equipe = 4;
UPDATE equipe SET statut_approbation = 'APPROUVE' WHERE id_equipe = 5;
UPDATE equipe SET statut_approbation = 'APPROUVE' WHERE id_equipe = 6;

INSERT INTO messages(type_message, text, lien)
VALUES ('INVITATION', 'Vous etes invite(e) a rejoindre l''equipe ', 1);

INSERT INTO messages(type_message, text)
VALUES ('TEXTE', 'Rappel: La periode d''inscription se termine le 15 septembre.');

INSERT INTO membre_message(cip, id_message)
VALUES ('carm1234', 1),
       ('carm1234', 2);

/* RULES */
create or replace rule updateStatutMembreMatchView as on update to membre_match_view
do instead update membre_match set statut_membre_match = new.statut_membre_match
           where cip = new.cip and id_match = new.id_match;

create or replace rule insertIntoMembreMatchView as on insert to membre_match_view
do instead insert into membre_match(cip, id_match, statut_membre_match)
           values(new.cip, new.id_match, new.statut_membre_match);

create or replace rule deleteFromMembreMatchView as on delete to membre_match_view
do instead delete from membre_match where cip = old.cip and id_match = old.id_match;

create or replace rule updateStatutEquipeMatchView as on update to match_view
do instead update equipe_match set statut_equipe_match = new.statut_equipe_match
           where id_equipe = new.id_equipe and id_match = new.id_match;

create or replace rule insertIntoEquipeMatch as on insert to match_view
do instead insert into equipe_match (ID_EQUIPE, ID_MATCH, STATUT_EQUIPE_MATCH, COTE)
           values (new.id_equipe, new.id_match, new.statut_equipe_match, new.cote);

create or replace rule deleteFromEquipeMatch as on delete to match_view
do instead delete from equipe_match where id_equipe = old.id_equipe and id_match = old.id_match;




